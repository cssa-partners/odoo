# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import datetime
from datetime import timedelta, date

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class Mail(models.Model):
    _inherit = 'mail.message'

    sale_team = fields.Many2one('crm.team')

    def send(self, auto_commit=False, raise_exception=False):
        sending_user = self.env['res.users'].search([('partner_id','=',self.author_id.id)])
        self.sale_team = sending_user.sale_team_id.id
        return super(Mail, self).send()

class MailTemplate(models.Model):
    _inherit = 'mail.template'

    sale_team = fields.Many2one('crm.team')
    company = fields.Many2one('res.company')

    def migrate_sale_team(self):
        thread = threading.Thread(target=self.migrate_s_t, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_s_t(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_emails = new_env['mail.template'].search([])

                for mail in all_emails:
                    if mail.x_sale_team:
                        mail.sale_team = mail.x_sale_team.id

                print("Completed")