# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from datetime import date

import base64
import urllib.parse

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools



class Lead(models.Model):
    _inherit = 'crm.lead'

    to_rework = fields.Boolean('To rework', default=False)
    date_confirm_check = fields.Char('Data Conferma Check')
    date_checkup = fields.Char('Data Check-Up')
    time_checkup = fields.Char('Ora Check-Up')
    date_time_checkup = fields.Char('Ora Check-Up')
    address_checkup = fields.Char('Indirizzo')
    age_owners = fields.Char('Età titolari')
    esito = fields.Selection([('6', 'Check'),('15', 'Ripasso'),('30', 'Non Interessato'),('31', 'Assente'),('32', 'Non Qualifica'),('34', 'No Show'),('33', 'Non Decisionale'),('51', 'Gruppo')],'Esito')
    dipendenti = fields.Char('N° Dipendenti')
    fatturato = fields.Char('Fatturato')
    region = fields.Char('Regione')
    provincia = fields.Char('Provincia')
    settore = fields.Char('Settore')
    ateco = fields.Char("ATECO")
    area_interesse = fields.Char("Area d'interessse")
    categoria_interesse = fields.Char("Categoria d'interesse")
    azienda_cliente = fields.Char('Azienda Cliente')
    posizione_lavorativa = fields.Selection([
        ('CEO - SRL/SPA','CEO - SRL/SPA'),
        ('AMMINISTRATORE UNICO - SRL/SPA','AMMINISTRATORE UNICO - SRL/SPA'),
        ('SOCIO ACCOMANDATARIO - SAS','SOCIO ACCOMANDATARIO - SAS'),
        ('PRESIDENTE - SRL/SPA','PRESIDENTE - SRL/SPA'),
        ('TITOLARE - DITTA INDIVIDUALE','TITOLARE - DITTA INDIVIDUALE'),
        ('SOCIO - SNC','SOCIO - SNC'),
        ('AMMINISTRATORE DELEGATO - SRL/SPA','AMMINISTRATORE DELEGATO - SRL/SPA'),
        ('UFF. ACQUISTI','UFF. ACQUISTI'),
        ('UFF. TECNICO','UFF. TECNICO'),
        ('SEGRETARIA/O','SEGRETARIA/O'),
        ('RESP. LABORATORIO','RESP. LABORATORIO'),
        ('RESP. PRODUZIONE','RESP. PRODUZIONE'),
        ('UFF. FORNITORI','UFF. FORNITORI'),
        ('DIPENDENTE','DIPENDENTE'),
        ('LABORATORIO','LABORATORIO'),
        ('ARCHITETTO','ARCHITETTO'),
        ('RESP. INTERIOR','RESP. INTERIOR'),
        ('AMMINISTRAZIONE','AMMINISTRAZIONE')
    ])

    forma_societaria = fields.Selection([
        ('Ditta Individuale','Ditta Individuale'),
        ('SNC','SNC'),
        ('SAS','SAS'),
        ('SRL','SRL'),
        ('SRLS','SRLS'),
        ('SPA','SPA')

    ], 'Forma Societaria')

    partita_iva = fields.Char('Partita IVA')
    codice_fiscale = fields.Char('Codice Fiscale')

    show_skype = fields.Boolean('Team')
    in_out = fields.Char('IN/OUT')
    in_out_analisi = fields.Char('IN/OUT Analisi')
    for_admin = fields.Boolean('Amministrazione')

    activity_orario = fields.Char('Orario')
    call_count = fields.Integer('Call Count')
    check_count = fields.Integer('Check Count')
    go_count = fields.Integer('Go Count')

    is_telemarketing = fields.Boolean('Is telemarketing')

    caratteristiche_titolare = fields.Text('Caratteristiche Titolare')
    motivo_firma = fields.Text('Motivo Firma')
    persone_chiave = fields.Text('Persone Chiave')
    promesse_firma = fields.Text('Promesse alla Firma')
    situazione_finanziaria = fields.Text('Situazione Finanziaria')
    indicazioni_stradali = fields.Text('Indicazioni Stradali')

    saldo_check = fields.Char()
    percentuale_anticipo = fields.Char()
    saldo_anticipo = fields.Char()
    importo_progetto = fields.Float()
    importo_spese = fields.Float()
    n_rate = fields.Char()
    user_check_email = fields.Char()
    link_partner = fields.Char()

    orario = fields.Char('Orario')

    cliente = fields.Many2one('res.users', string='Cliente', index=True, tracking=True, domain="[('groups_id.name','=','Cliente')]")

    commerciale = fields.Many2one('res.users', string='Commerciale', index=True, tracking=True, domain="[('sale_team_id','=',9)]")

    commerciale_affiancato = fields.Many2one('res.users', string='Commerciale Affiancato', index=True, tracking=True, domain="[('sale_team_id','=',9)]")

    analista = fields.Many2one('res.users', string='Analista', index=True, tracking=True, domain="[('sale_team_id','=',10)]")

    analista_affiancato = fields.Many2one('res.users', string='Analista Affiancato', index=True, tracking=True, domain="[('sale_team_id','=',10)]")

    teleseller = fields.Many2one('res.users', string='Teleseller', index=True, tracking=True, domain="[('sale_team_id','=',8)]")


    # Tecnici
    tecnico = fields.Many2one('res.users', string='Tecnico Management', index=True, tracking=True, domain="[('sale_team_id','=',13)]")

    tecnico_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tutor = fields.Many2one('res.users', string='Tutor Management', index=True, tracking=True, domain="[('sale_team_id','=',13)]")

    tutor_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tecnico_certificazioni = fields.Many2one('res.users', string='Tecnico Certificazioni', index=True, tracking=True, domain="[('sale_team_id','=',15)]")

    tecnico_certificazioni_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tutor_certificazioni = fields.Many2one('res.users', string='Tutor Certificazioni', index=True, tracking=True, domain="[('sale_team_id','=',15)]")

    tutor_certificazioni_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tecnico_marketing = fields.Many2one('res.users', string='Tecnico Marketing', index=True, tracking=True, domain="[('sale_team_id','=',1)]")

    tecnico_marketing_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tutor_marketing = fields.Many2one('res.users', string='Tutor Marketing', index=True, tracking=True, domain="[('sale_team_id','=',1)]")

    tutor_marketing_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tecnico_software = fields.Many2one('res.users', string='Tecnico Software', index=True, tracking=True, domain="[('sale_team_id','=',11)]")

    tecnico_software_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    tutor_software = fields.Many2one('res.users', string='Tutor Software', index=True, tracking=True, domain="[('sale_team_id','=',11)]")

    tutor_software_payment_term_id = fields.Many2one('account.payment.term', string='Termini di pagamento', domain="[('is_tecnico', '=', True)]")

    partner_1 = fields.Many2one('res.users', string='Partner 1', index=True, tracking=True, domain="[('sale_team_id','=',14)]")

    partner_2 = fields.Many2one('res.users', string='Partner 2', index=True, tracking=True, domain="[('sale_team_id','=',14)]")

    partner_3 = fields.Many2one('res.users', string='Partner 3', index=True, tracking=True, domain="[('sale_team_id','=',14)]")

    partner_4 = fields.Many2one('res.users', string='Partner 4', index=True, tracking=True, domain="[('sale_team_id','=',14)]")

    project_manager = fields.Many2one('res.users', string='Project Manager', index=True, tracking=True)

    check_contract_1 = fields.Char('Contratto Check')
    check_contract_2 = fields.Char('Privacy')

    now = fields.Char()

    check_doc_1 = fields.Char("Documento 1")
    check_doc_2 = fields.Char("Documento 2")
    check_doc_3 = fields.Char("Documento 3")
    check_doc_4 = fields.Char("Documento 4")

    date_last_appointment = fields.Datetime('Data ultimo appuntamento')
    date_analisi = fields.Datetime('Data prevista analisi')

    analisi_annullata = fields.Boolean('Annullata')

    @api.model
    def create(self, vals):
        context = dict(self._context or {})
        if vals.get('provincia'):
            context['provincia'] = vals.get('provincia')
            self.partner_id.provincia = vals.get('provincia')
        if vals.get('region'):
            context['region'] = vals.get('region')
            self.partner_id.region = vals.get('region')
        if vals.get('partita_iva'):
            context['vat'] = vals.get('partita_iva')
            self.partner_id.partita_iva = vals.get('partita_iva')
            self.partner_id.vat = vals.get('partita_iva')
        if vals.get('settore'):
            context['settore'] = vals.get('settore')
            self.partner_id.settore = vals.get('settore')
        if vals.get('ateco'):
            context['ateco'] = vals.get('ateco')
            self.partner_id.ateco = vals.get('ateco')
        if vals.get('area_interesse'):
            context['area_interesse'] = vals.get('area_interesse')
            self.partner_id.area_interesse = vals.get('area_interesse')
        if vals.get('categoria_interesse'):
            context['categoria_interesse'] = vals.get('categoria_interesse')
            self.partner_id.categoria_interesse = vals.get('categoria_interesse')
        if vals.get('dipendenti'):
            context['dipendenti'] = vals.get('dipendenti')
            self.partner_id.dipendenti = vals.get('dipendenti')
        if vals.get('fatturato'):
            context['fatturato'] = vals.get('fatturato')
            self.partner_id.fatturato = vals.get('fatturato')
        if vals.get('azienda_cliente'):
            context['azienda_cliente'] = vals.get('azienda_cliente')
            self.partner_id.azienda_cliente = vals.get('azienda_cliente')
            for child in self.partner_id.child_ids:
                child.azienda_cliente = vals.get('azienda_cliente')

        return super(Lead, self).create(vals)

    def write(self, vals):
        is_manager = False

        for user_group in self.env.user.groups_id:
            if user_group.category_id.name == 'Dipendente' and user_group.name == 'Manager':
                is_manager = True
                break

        self.notify('followers', vals)

        if self.env.user.name != self.user_id.name and vals.get('stage_id') and not is_manager:
            vals = None
            return vals
        elif not is_manager and self.env.user.sale_team_id.id == 9 and vals.get('stage_id') == 9:
            vals = None
            return vals
        else:
            if vals.get('website'):
                vals['website'] = self.env['res.partner']._clean_website(vals['website'])

            # stage change: update date_last_stage_update
            if 'stage_id' in vals:
                vals['date_last_stage_update'] = fields.Datetime.now()
                stage_id = self.env['crm.stage'].browse(vals['stage_id'])
                self.manage_in_out(stage_id)

                if stage_id.id == 5:
                    self.show_skype = False
                elif stage_id.id == 43:
                    self.cancel_opportunity()

                vals.update({'probability': 50})
                if stage_id.is_won:
                    vals.update({'probability': 100})
                    self.esito = False

                    if vals.get('stage_id') == 6:
                        tag_check = self.env['res.partner.category'].search([('name','=','Cliente Check')]).id
                        #self.partner_id.category_id = [tag_check]
                        self.notify('check', None)
                    # elif vals.get('stage_id') == 24:
                    #     tag_go = self.env['res.partner.category'].search([('name','=','Cliente')]).id
                    #     self.partner_id.category_id = [tag_go]
                    #
                    #     self.partner_id.customer_rank = 1
                    #
                    #     tag_apertura_lavori = self.env['crm.lead.tag'].search([('name','=','Apertura lavori')]).id
                    #     self.tag_ids = [tag_apertura_lavori]
                    #
                    #     for opportunity in self.partner_id.opportunity_ids:
                    #         if opportunity.team_id.name == 'Commerciali':
                    #             opportunity.tag_ids = [30]
                    #             break
                    #
                    #     self.partner_id.category_id = [tag_go]
                    #     for child in self.partner_id.child_ids:
                    #         child.category_id = [tag_go]
                    #
                    #     for opportunity in self.partner_id.opportunity_ids:
                    #         if opportunity.team_id.name == 'Commerciali':
                    #             opportunity.tag_ids = [30]
                    #             self.notify('go', None)
                    #             break

                # elif not stage_id.is_won:
                #     if stage_id.name == 'No GO':
                #         for opportunity in self.partner_id.opportunity_ids:
                #             if opportunity.team_id.name == 'Commerciali':
                #                 opportunity.tag_ids = [31]
                #                 for order in opportunity.order_ids:
                #                     order.esito = 'No Go'
                #                 break

                if stage_id.id == 39:
                    self.request_check_docs()


            # Only write the 'date_open' if no salesperson was assigned.
            if vals.get('user_id') and 'date_open' not in vals and not self.mapped('user_id'):
                vals['date_open'] = fields.Datetime.now()
            # stage change with new stage: update probability and date_closed
            if vals.get('probability', 0) >= 100 or not vals.get('active', True):
                vals['date_closed'] = fields.Datetime.now()
            elif 'probability' in vals:
                vals['date_closed'] = False
            if vals.get('user_id') and 'date_open' not in vals:
                vals['date_open'] = fields.Datetime.now()

            write_result = super(Lead, self).write(vals)
            # Compute new automated_probability (and, eventually, probability) for each lead separately
            if self._should_update_probability(vals):
                self._update_probability()

            if vals.get('commerciale_affiancato') or vals.get('analista_affiancato') or vals.get('analista') or vals.get('commerciale') or vals.get('commerciale_affiancato'):
                # update appointments if any
                self.update_appointment_owner()

            return write_result


    def get_directions(self):
        query = self.partner_id.city+","+self.partner_id.street+","+self.partner_id.zip
        encoded_query = urllib.parse.quote(query)
        google_map_url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination="+encoded_query

        return {
            'name'     : 'Go to website',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'url'      : google_map_url
       }

    def request_check_docs(self):
        current_user = self.env.user
        current_company = self.env['res.company'].search([('id','=',current_user.company_id.id)])

        if self.forma_societaria == 'SRL' or self.forma_societaria == 'SRLS' or self.forma_societaria == 'SPA':
            self.check_doc_1 = current_company.società_check_doc_1
            self.check_doc_2 = current_company.società_check_doc_2
            self.check_doc_3 = current_company.società_check_doc_3
            self.check_doc_4 = current_company.società_check_doc_4
        else:
            self.check_doc_1 = current_company.ditta_check_doc_1
            self.check_doc_2 = current_company.ditta_check_doc_2
            self.check_doc_3 = current_company.ditta_check_doc_3
            self.check_doc_4 = current_company.ditta_check_doc_4

        temp_now = date.today()
        self.now = "Milano, " + str(temp_now.day) + "/" + str(temp_now.month) + "/" + str(temp_now.year)

        #run the report and get the pdf (use the reports xml_id and pass record ids into render)
        data, data_format = self.env.ref('cssa.report_request_check_doc').render([self.id])

        #save the attachment
        att_id = self.env['ir.attachment'].create({
            'name': 'CONFERMA CHECK UP PRELIMINARE.pdf',
            'type': 'binary',
            'datas': base64.encodestring(data),
            'res_model': 'crm.lead',
            'res_id': self.id,
            'mimetype': 'application/pdf'
        })

        mail_template = self.env.ref('cssa.request_check_docs_template')
        mail_template.attachment_ids = [(6,0,[att_id.id])]
        mail_template.send_mail(self.id, force_send=True)

    def notify(self, type, vals):
        if type == 'followers':
            if self.env.user.sale_team_id.id == 9 and vals.get('stage_id') == 31:
                title = "Nuovo assente"
                message = "Questo contatto era assente"

                followers=self.message_partner_ids
                follower_ids=(tuple(followers.ids))

                self.message_notify(
                    partner_ids=follower_ids,
                    subject=title,
                    body=message
                )
        elif type == 'check':
            check_template = self.env.ref('cssa.check_template')
            check_group = self.sudo().env['res.groups'].search(['&',('category_id.name','=','Check'),('name','=','User')])
            self.link_partner = 'https://mdcdigitale.cloud/web#id='+str(self.partner_id.id)+'&action=129&model=res.partner&view_type=form&cids=1&menu_id=100'

            for user in check_group.users:
                self.user_check_email = user.email
                check_template.send_mail(self.id, force_send=True)

            # message = "C'è stato un nuovo Check!"
            # users = []
            # groups=self.sudo().env['res.groups'].search([])
            # for group in groups:
            #     if group.category_id.name == 'Check' and group.name == 'User':
            #         users = group.users
            #         break
            #
            # user_ids = []
            # for user in users:
            #     user_ids.append(user.id)
            #
            # user_ids = (tuple(user_ids))
            # self.message_notify(
            #     partner_ids=user_ids,
            #     subject='Nuovo Check',
            #     body=message
            # )
        # elif type == 'go':
        #     message = self.name
        #
        #     user_ids = []
        #     if self.commerciale:
        #         user_ids.append(self.commerciale.partner_id.id)
        #     if self.commerciale_affiancato:
        #         user_ids.append(self.commerciale_affiancato.partner_id.id)
        #
        #     user_ids = (tuple(user_ids))
        #     print("user_ids",user_ids)
        #     self.message_notify(
        #         partner_ids=user_ids,
        #         subject='Nuovo Go',
        #         body=message
        #     )

    def rework_zone(self):
        completed_zone = self.env['cssa.completed_zones'].search([('opportunity_id', '=', self.id)])
        completed_zone.unlink()

        self.user_id = self.env.user.id
        self.to_rework = False

    def register_skype_activity(self):
        skip_activity = False

        for act in self.activity_ids:
            if act.activity_type_id.id == 2 and act.state == 'overdue' and act.state == 'today' and act.state == 'planned':
                skip_activity = True
                break

        if not skip_activity:
            activity = self.env['mail.activity']
            activity.create({
                'res_id': self.id,
                'res_model_id': 245,
                'res_model': 'crm.lead',
                'activity_type_id': 2,
                'date_deadline': date.today(),
                'activity_category': 'default',
                'previous_activity_type_id': False,
                'recommended_activity_type_id': False,
                'user_id': self.user_id.id
            }).action_feedback()

    def action_call_telephone(self):
        if self.phone or self.partner_id.phone:
            self.register_skype_activity()
            if self.partner_id.phone:
                skype_number = 'skype:' + self.partner_id.phone + '?call'
            else:
                skype_number = 'skype:' + self.phone + '?call'
            return {
                'type': 'ir.actions.act_url',
                'url': skype_number,
                'target': 'new',
            }

    def action_call_mobile(self):
        if self.mobile or self.partner_id.mobile:
            self.register_skype_activity()
            if self.partner_id.mobile:
                skype_number = 'skype:' + self.partner_id.mobile + '?call'
            else:
                skype_number = 'skype:' + self.mobile + '?call'
            return {
                'type': 'ir.actions.act_url',
                'url': skype_number,
                'target': 'new',
            }

    def action_new_appointment(self):
        Lead = self.action_assign_new_opportunity(9)
        return {
            'name': 'Lead',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'domain': [('type', '=', Lead.type)],
            'res_id': Lead.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {'default_type': Lead.type}
        }

    def action_new_analisi(self):
        Lead = self.action_assign_new_opportunity(19)
        return {
            'name': 'Lead',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'domain': [('type', '=', Lead.type)],
            'res_id': Lead.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {'default_type': Lead.type}
        }

    # def action_new_job(self):
    #     Lead = self.action_assign_new_opportunity(47)
    #     return {
    #         'name': 'Lead',
    #         'view_mode': 'form',
    #         'res_model': 'crm.lead',
    #         'domain': [('type', '=', Lead.type)],
    #         'res_id': Lead.id,
    #         'view_id': False,
    #         'type': 'ir.actions.act_window',
    #         'context': {'default_type': Lead.type}
    #     }


    # Template function to show an alert
    def show_alert(self):
        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = "Prima di selezionare Check devi inserire il fatturato e indirizzo email"
        return {
            'name': 'Errore',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'context': context
        }

    def _onchange_partner_id_values(self, partner_id):
        """ returns the new values when partner_id has changed """
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)

            partner_name = partner.parent_id.name
            if not partner_name and partner.is_company:
                partner_name = partner.name

            contact_name = False
            contact_title = ''
            contact_function = ''
            if partner.is_company == False:
                contact_name = partner.name
                contact_title = partner.title.id
                contact_function = partner.posizione_lavorativa
            elif len(partner.child_ids) > 0:
                contact_name = partner.child_ids[0].name
                contact_title = partner.child_ids[0].title.id
                contact_function = partner.child_ids[0].posizione_lavorativa

            return {
                'partner_name': partner_name,
                'contact_name': contact_name,
                'title': contact_title,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id.id,
                'country_id': partner.country_id.id,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile': partner.mobile,
                'zip': partner.zip,
                'function': partner.function,
                'posizione_lavorativa': contact_function,
                'website': partner.website,
                'dipendenti': partner.dipendenti
            }
        return {}

    def _create_lead_partner_data(self, name, is_company, parent_id=False):
        """ extract data from lead to create a partner
            :param name : furtur name of the partner
            :param is_company : True if the partner is a company
            :param parent_id : id of the parent partner (False if no parent)
            :returns res.partner record
        """
        email_split = tools.email_split(self.email_from)
        res = {
            'name': name,
            'user_id': self.env.context.get('default_user_id') or self.user_id.id,
            'comment': self.description,
            'team_id': self.team_id.id,
            'parent_id': parent_id,
            'phone': self.phone,
            'mobile': self.mobile,
            'email': email_split[0] if email_split else False,
            'title': self.title.id,
            'street': self.street,
            'street2': self.street2,
            'zip': self.zip,
            'city': self.city,
            'country_id': self.country_id.id,
            'state_id': self.state_id.id,
            'website': self.website,
            'is_company': is_company,
            'type': 'contact',
            'provincia': self.provincia,
            'region': self.region,
            'vat': self.partita_iva,
            'partita_iva': self.partita_iva,
            'settore': self.settore,
            'area_interesse': self.area_interesse,
            'ateco': self.ateco,
            'categoria_interesse': self.categoria_interesse,
            'dipendenti': self.dipendenti,
            'fatturato': self.fatturato,
            'azienda_cliente': self.azienda_cliente,
            'function': self.posizione_lavorativa
        }
        if self.lang_id:
            res['lang'] = self.lang_id.code
        return res

    def manage_in_out(self, stage_id):
        if stage_id.id == 30 or stage_id.id == 6 or stage_id.id == 15 or stage_id.id == 29:
            self.in_out = 'IN'
        elif stage_id.id == 33 or stage_id.id == 31 or stage_id.id == 32 or stage_id.id == 34:
            self.in_out = 'OUT'

        # manage in/out analisi
        if stage_id.id == 24:
            self.in_out_analisi = 'IN'
        elif stage_id.id == 42:
            self.in_out_analisi = 'OUT'

    def cancel_opportunity(self):
        for opportunity in self.partner_id.opportunity_ids:
            if opportunity.team_id.id == 9:
                self.env['calendar.event'].search([('opportunity_id.id', '=', opportunity.id)]).unlink()
                opportunity.unlink()
                break

    def action_assign_new_opportunity(self, stage_id):
        value = {
            'planned_revenue': self.planned_revenue,
            'name': self.name,
            'partner_id': self.partner_id.id,
            'type': 'opportunity',
            'date_open': fields.Datetime.now(),
            'email_from': self.partner_id and self.partner_id.email or self.email_from,
            'phone': self.partner_id and self.partner_id.phone or self.phone,
            'date_conversion': fields.Datetime.now(),
            'stage_id': stage_id,
            'activity_ids': None,
            'message_ids': None,
            'settore': self.settore,
            'region': self.region,
            'provincia': self.provincia,
            'tecnico': False,
            'tutor': False,
            'tecnico_certificazioni': False,
            'tutor_certificazioni': False,
            'tecnico_marketing': False,
            'tutor_marketing': False,
            'tecnico_software': False,
            'tutor_software': False,
            'partner_1': False,
            'partner_2': False,
            'partner_3': False,
            'partner_4': False,
            'project_manager': False
        }

        current_user = self.env.user
        if stage_id == 9:
            value['user_id'] = current_user.commerciale.id
            value['teleseller'] = self.user_id.id
            value['commerciale'] = current_user.commerciale.id
            value['commerciale_affiancato'] = False
            value['analista'] = False
            value['analista_affiancato'] = False

            self.teleseller = self.user_id.id

        #elif stage_id == 19 or stage_id == 47:
        elif stage_id == 19:
            value['caratteristiche_titolare'] = self.caratteristiche_titolare
            value['motivo_firma'] = self.motivo_firma
            value['persone_chiave'] = self.persone_chiave
            value['promesse_firma'] = self.promesse_firma
            value['situazione_finanziaria'] = self.situazione_finanziaria
            value['indicazioni_stradali'] = self.indicazioni_stradali
            value['age_owners'] = self.age_owners
            value['date_confirm_check'] = self.date_confirm_check
            value['date_checkup'] = self.date_checkup
            value['time_checkup'] = self.time_checkup
            value['fatturato'] = self.fatturato
            value['dipendenti'] = self.dipendenti
            value['address_checkup'] = self.address_checkup

            value['teleseller'] = self.teleseller.id
            value['commerciale'] = self.commerciale.id
            value['commerciale_affiancato'] = self.commerciale_affiancato.id

            value['analista'] = self.env.user.id
            value['team_id'] = self.env.user.sale_team_id.id
            value['analista_affiancato'] = False

            value['partita_iva'] = self.partita_iva
            value['codice_fiscale'] = self.codice_fiscale
            value['fatturato'] = self.fatturato

            value['forma_societaria'] = self.forma_societaria

            current_user.commerciale = self.commerciale.id

            attachments = self.env['ir.attachment'].search([('res_id','=',self.id)])
            if len(attachments) > 0:
                value['check_contract_1'] = attachments[0].local_url
            if len(attachments) > 1:
                value['check_contract_2'] = attachments[1].local_url

        # elif stage_id == 47:
        #     value['analista'] = self.analista.id
        #     value['analista_affiancato'] = self.analista_affiancato.id
        #     value['tecnico'] = self.tecnico.id
        #     value['tutor'] = self.tutor.id
        #     value['tecnico_certificazioni'] = self.tecnico_certificazioni.id
        #     value['tutor_certificazioni'] = self.tutor_certificazioni.id
        #     value['tecnico_marketing'] = self.tecnico_marketing.id
        #     value['tutor_marketing'] = self.tutor_marketing.id
        #     value['tecnico_software'] = self.tecnico_software.id
        #     value['tutor_software'] = self.tutor_software.id
        #     value['partner_1'] = self.partner_1.id
        #     value['partner_2'] = self.partner_2.id
        #     value['partner_3'] = self.partner_3.id
        #     value['partner_4'] = self.partner_4.id
        #     value['partner_5'] = self.partner_5.id
        #     value['partner_6'] = self.partner_6.id
        #     value['partner_7'] = self.partner_7.id
        #     value['partner_8'] = self.partner_8.id
        #     value['partner_9'] = self.partner_9.id
        #     value['partner_10'] = self.partner_10.id

        return self.env['crm.lead'].create(value)


    def confirm_contact_data(self):
        self.partner_id.vat = "IT"+self.partita_iva
        self.partner_id.partita_iva = "IT"+self.partita_iva
        self.partner_id.l10n_it_codice_fiscale = self.codice_fiscale
        self.partner_id.fatturato = self.fatturato
        self.partner_id.email = self.email_from

        if self.stage_id.name == 'Check':
            tag_check = self.env['res.partner.category'].search([('name','=','Cliente Check')]).id
            self.partner_id.category_id = [tag_check]
            self.partner_id.relationship = 'Cliente'
            self.partner_id.customer_rank = 1
            
            for child in self.partner_id.child_ids:
                child.category_id = [tag_check]

        self.partner_id.name = self.partner_id.name.upper()
        if self.forma_societaria not in self.partner_id.name:
            self.partner_id.name = self.partner_id.name + " " + self.forma_societaria

        self.updateInfoAnalista('date_confirm_check')
        self.updateInfoAnalista('time_checkup')
        self.updateInfoAnalista('address_checkup')
        self.updateInfoAnalista('motivo_firma')
        self.updateInfoAnalista('promesse_firma')
        self.updateInfoAnalista('caratteristiche_titolare')
        self.updateInfoAnalista('age_owners')
        self.updateInfoAnalista('situazione_finanziaria')
        self.updateInfoAnalista('persone_chiave')
        self.updateInfoAnalista('indicazioni_stradali')


    @api.onchange('partita_iva')
    def _onchange_partita_iva(self):
        for lead in self.partner_id.opportunity_ids:
            lead.partita_iva = self.partita_iva

    @api.onchange('esito')
    def _onchange_esito(self):
        if self._origin.esito == '15':
            ripasso_tag = self.env['crm.lead.tag'].search([('name','=','Ripasso')]).id
            self.tag_ids = [ripasso_tag]
        self.stage_id = int(self.esito)

    @api.onchange('commerciale')
    def _onchange_commerciale(self):
        current_user = self.env.user
        current_user.commerciale = self.commerciale

        if self.commerciale:
            if self.team_id.id == 9:
                self.user_id = self.commerciale.id

            # update orders if any
            self.update_collaborator(self._origin.order_ids, self.commerciale, 'commerciale', 121)

            thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.commerciale.id, 'commerciale'))
            thread.daemon = True                            # Daemonize thread
            thread.start()

    @api.onchange('commerciale_affiancato')
    def _onchange_commerciale_affiancato(self):
        self.update_collaborator_affiancato(self._origin.order_ids, self.commerciale, self.commerciale_affiancato, 'commerciale_affiancato', 121)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.commerciale_affiancato.id, 'commerciale_affiancato'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def update_appointment_owner(self):
        for activity in self.activity_ids:
            if activity.calendar_event_id:
                appuntamento = self.env['calendar.event'].search([('id','=',activity.calendar_event_id.id)])

                if self.team_id.id == 9:
                    appuntamento.user_id = self.commerciale.id

                    if self.commerciale_affiancato:
                        appuntamento.partner_ids = [self.commerciale.partner_id.id, self.commerciale_affiancato.partner_id.id]
                    else:
                        appuntamento.partner_ids = [self.commerciale.partner_id.id]
                elif self.team_id.id == 10:
                    appuntamento.user_id = self.analista.id

                    if self.analista_affiancato:
                        appuntamento.partner_ids = [self.analista.partner_id.id, self.analista_affiancato.partner_id.id]
                    else:
                        appuntamento.partner_ids = [self.analista.partner_id.id]


    def job_related_users(self, jobid):
        if jobid:
            empids = self.env['hr.employee'].search([('user_id', '!=', False), ('job_id', '=', jobid.id)])
            return [emp.user_id.id for emp in empids]
        return False

    def find_comm_member(self, order, user):
        member_lst = []
        emp_id = self.env['hr.employee'].search([('user_id', '=', user.id)], limit=1)

        for soline in order.order_line:
            for lineid in soline.product_id.product_comm_ids:
                lines_user = {'user_id': user.id, 'job_id': emp_id.job_id.id, 'name_commission': lineid.product_id.name}
                if lineid.user_ids and user.id in [user.id for user in lineid.user_ids]:
                    lines_user[
                        'commission'] = soline.price_subtotal * lineid.commission / 100 if lineid.compute_price_type == 'per' else lineid.commission * soline.product_uom_qty
                    member_lst.append(lines_user)
                elif lineid.job_id and not lineid.user_ids:
                    if user.id in self.job_related_users(lineid.job_id):
                        lines_user[
                            'commission'] = soline.price_subtotal * lineid.commission / 100 if lineid.compute_price_type == 'per' else lineid.commission * soline.product_uom_qty
                        member_lst.append(lines_user)

        return member_lst

    def remove_employee_comm(self, employee_type, group_id, user_id):
        new_user = self.env['res.users'].search([('id','=',user_id)])

        for order in self._origin.order_ids:
            for commission in order.sale_order_comm_ids:
                for user in commission.user_id:
                    for group in user.groups_id:
                        if group.id == group_id:
                            commission.unlink()

            if new_user:
                member_lst = self.find_comm_member(order, new_user)
                commissione = 0
                for member in member_lst:
                    commissione += member['commission']

                vals = {
                    "user_id": new_user.id,
                    "job_id": new_user.employee_id.job_id.id,
                    "commission": commissione,
                    "order_id": order.id
                }
                self.env['sales.order.commission'].create(vals)
                self.add_new_follower(new_user)

        if employee_type == 'tecnico':
            self.tutor = False
        elif employee_type == 'tecnico_certificazioni':
            self.tutor_certificazioni = False
        elif employee_type == 'tecnico_marketing':
            self.tutor_marketing = False
        elif employee_type == 'tecnico_software':
            self.tutor_software = False
        elif employee_type == 'analista':
            self.analista_affiancato = False
        elif employee_type == 'commerciale':
            self.commerciale_affiancato = False
        elif employee_type == 'tutor':
            self.tutor = False
        elif employee_type == 'tutor_certificazioni':
            self.tutor_certificazioni = False
        elif employee_type == 'tutor_marketing':
            self.tutor_marketing = False
        elif employee_type == 'tutor_software':
            self.tutor_software = False



    def update_collaborator(self, orders, collaborator, user_type, group_id):
        if collaborator:
            for order in orders:
                member_lst = self.find_comm_member(order, collaborator)

                commission_collaborator = 0
                for member in member_lst:
                    commission_collaborator += member['commission']

                sales_order_commission = self.env['sales.order.commission'].search([('order_id','=',order.id)])
                for commission in sales_order_commission:
                    if commission.job_id.id == collaborator.employee_id.job_id.id:
                        commission.user_id = collaborator.id
                        commission.commission = commission_collaborator
                        break

        else:
            self.remove_employee_comm(user_type, group_id, collaborator.id)

    def update_collaborator_affiancato(self, orders, collaborator, collaborator_affiancato, user_type, group_id):
        if collaborator_affiancato:
            for order in orders:
                commission_collaborator = 0
                commission_collaborator_affiancato = 0

                collaborator_lst = self.find_comm_member(order, collaborator)
                collaborator_affiancato_lst = self.find_comm_member(order, collaborator_affiancato)

                for member in collaborator_lst:
                    commission_collaborator += member['commission']

                for member in collaborator_affiancato_lst:
                    commission_collaborator_affiancato += member['commission']

                commission_collaborator /= 2
                commission_collaborator_affiancato /= 2

                n_collaborators = 0
                collaborator_affiancato_found = False
                sales_order_commission = self.env['sales.order.commission'].search([('order_id','=',order.id)])
                for commission in sales_order_commission:
                    if commission.user_id.id == collaborator.id:
                        commission.commission = commission_collaborator
                    if commission.user_id.id == collaborator_affiancato.id:
                        commission.commission = commission_collaborator_affiancato
                    if commission.job_id.id == collaborator_affiancato.employee_id.job_id.id:
                        n_collaborators += 1

                    if n_collaborators == 2:
                        commission.user_id = collaborator_affiancato.id
                        collaborator_affiancato_found = True
                        self.add_new_follower(collaborator_affiancato)

                if not collaborator_affiancato_found:
                    vals = {
                        "user_id": collaborator_affiancato.id,
                        "job_id": collaborator_affiancato.employee_id.job_id.id,
                        "commission": commission_collaborator_affiancato,
                        "order_id": order.id
                    }
                    self.env['sales.order.commission'].create(vals)
                    self.add_new_follower(collaborator_affiancato)

        else:
            self.remove_employee_comm(user_type, group_id, collaborator.id)

    @api.onchange('teleseller')
    def _onchange_teleseller(self):
        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.teleseller.id, 'teleseller'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tecnico')
    def _onchange_tecnico(self):
        self.update_collaborator(self._origin.order_ids, self.tecnico, 'tecnico', 142)
        self.add_new_follower(self.tecnico)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tecnico.id, 'tecnico'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def add_participants(self, opportunity_id, user_id, user_type):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_orders = new_env['sale.order'].search([('opportunity_id','=', opportunity_id)])
                for order in all_orders:
                    if user_type == 'commerciale':
                        order.commerciale = user_id
                    elif user_type == 'commerciale_affiancato':
                        order.commerciale_affiancato = user_id
                    elif user_type == 'analista':
                        order.analista = user_id
                    elif user_type == 'analista_affiancato':
                        order.analista_affiancato = user_id
                    elif user_type == 'tecnico':
                        order.tecnico = user_id
                    elif user_type == 'tecnico_certificazioni':
                        order.tecnico_certificazioni = user_id
                    elif user_type == 'tecnico_marketing':
                        order.tecnico_marketing = user_id
                    elif user_type == 'tecnico_software':
                        order.tecnico_software = user_id
                    elif user_type == 'tutor':
                        order.tutor = user_id
                    elif user_type == 'tutor_certificazioni':
                        order.tutor_certificazioni = user_id
                    elif user_type == 'tutor_marketing':
                        order.tutor_marketing = user_id
                    elif user_type == 'tutor_software':
                        order.tutor_software = user_id
                    elif user_type == 'teleseller':
                        order.teleseller = user_id


    @api.onchange('tecnico_certificazioni')
    def _onchange_tecnico_certificazioni(self):
        self.update_collaborator(self._origin.order_ids, self.tecnico_certificazioni, 'tecnico_certificazioni', 142)
        self.add_new_follower(self.tecnico_certificazioni)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tecnico_certificazioni.id, 'tecnico_certificazioni'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tecnico_marketing')
    def _onchange_tecnico_marketing(self):
        self.update_collaborator(self._origin.order_ids, self.tecnico_marketing, 'tecnico_marketing', 142)
        self.add_new_follower(self.tecnico_marketing)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tecnico_marketing.id, 'tecnico_marketing'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tecnico_software')
    def _onchange_tecnico_software(self):
        self.update_collaborator(self._origin.order_ids, self.tecnico_software, 'tecnico_software', 142)
        self.add_new_follower(self.tecnico_software)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tecnico_software.id, 'tecnico_software'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('partner_1')
    def _onchange_partner_1(self):
        self.update_collaborator(self._origin.order_ids, self.partner_1, 'partner_1', 142)
        self.add_new_follower(self.partner_1)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.partner_1.id, 'partner_1'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('partner_2')
    def _onchange_partner_2(self):
        self.update_collaborator(self._origin.order_ids, self.partner_2, 'partner_2', 142)
        self.add_new_follower(self.partner_2)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.partner_2.id, 'partner_2'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('partner_3')
    def _onchange_partner_3(self):
        self.update_collaborator(self._origin.order_ids, self.partner_3, 'partner_3', 142)
        self.add_new_follower(self.partner_3)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.partner_3.id, 'partner_3'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('partner_4')
    def _onchange_partner_4(self):
        self.update_collaborator(self._origin.order_ids, self.partner_4, 'partner_4', 142)
        self.add_new_follower(self.partner_4)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.partner_4.id, 'partner_4'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tutor')
    def _onchange_tutor(self):
        self.update_tutor(self.tutor, self.tecnico, 'tutor')
        self.add_new_follower(self.tutor)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tutor.id, 'tutor'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tutor_certificazioni')
    def _onchange_tutor_certificazioni(self):
        self.update_tutor(self.tutor_certificazioni, self.tecnico_certificazioni, 'tutor_certificazioni')
        self.add_new_follower(self.tutor_certificazioni)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tutor_certificazioni.id, 'tutor_certificazioni'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tutor_marketing')
    def _onchange_tutor_marketing(self):
        self.update_tutor(self.tutor_marketing, self.tecnico_marketing, 'tutor_marketing')
        self.add_new_follower(self.tutor_marketing)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tutor_marketing.id, 'tutor_marketing'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    @api.onchange('tutor_software')
    def _onchange_tutor_software(self):
        self.update_tutor(self.tutor_software, self.tecnico_software, 'tutor_software')
        self.add_new_follower(self.tutor_software)

        thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.tutor_software.id, 'tutor_software'))
        thread.daemon = True                            # Daemonize thread
        thread.start()

    ### On change Info Analista ###
    @api.onchange('date_confirm_check')
    def _onchange_date_confirm_check(self):
        self.updateInfoAnalista('date_confirm_check')

    @api.onchange('date_checkup')
    def _onchange_date_confirm_check(self):
        self.updateInfoAnalista('date_checkup')

    @api.onchange('time_checkup')
    def _onchange_time_checkup(self):
        self.updateInfoAnalista('time_checkup')

    @api.onchange('address_checkup')
    def _onchange_address_checkup(self):
        self.updateInfoAnalista('address_checkup')

    @api.onchange('motivo_firma')
    def _onchange_motivo_firma(self):
        self.updateInfoAnalista('motivo_firma')

    @api.onchange('promesse_firma')
    def _onchange_promesse_firma(self):
        self.updateInfoAnalista('promesse_firma')

    @api.onchange('caratteristiche_titolare')
    def _onchange_caratteristiche_titolare(self):
        self.updateInfoAnalista('caratteristiche_titolare')

    @api.onchange('age_owners')
    def _onchange_age_owners(self):
        self.updateInfoAnalista('age_owners')

    @api.onchange('situazione_finanziaria')
    def _onchange_situazione_finanziaria(self):
        self.updateInfoAnalista('situazione_finanziaria')

    @api.onchange('persone_chiave')
    def _onchange_persone_chiave(self):
        self.updateInfoAnalista('persone_chiave')

    @api.onchange('indicazioni_stradali')
    def _onchange_indicazioni_stradali(self):
        self.updateInfoAnalista('indicazioni_stradali')


    def updateInfoAnalista(self, field):
        for opportunity in self.partner_id.opportunity_ids:
            if opportunity.team_id.id == 10:
                if field == 'date_confirm_check':
                    opportunity.date_confirm_check = self.date_confirm_check
                elif field == 'date_checkup':
                    opportunity.date_checkup = self.date_checkup
                elif field == 'time_checkup':
                    opportunity.time_checkup = self.time_checkup
                elif field == 'address_checkup':
                    opportunity.address_checkup = self.address_checkup
                elif field == 'motivo_firma':
                    opportunity.motivo_firma = self.motivo_firma
                elif field == 'promesse_firma':
                    opportunity.promesse_firma = self.promesse_firma
                elif field == 'caratteristiche_titolare':
                    opportunity.caratteristiche_titolare = self.caratteristiche_titolare
                elif field == 'age_owners':
                    opportunity.age_owners = self.age_owners
                elif field == 'situazione_finanziaria':
                    opportunity.situazione_finanziaria = self.situazione_finanziaria
                elif field == 'persone_chiave':
                    opportunity.persone_chiave = self.persone_chiave
                elif field == 'indicazioni_stradali':
                    opportunity.indicazioni_stradali = self.indicazioni_stradali

                break

    def update_tutor(self, tutor, tecnico, user_type):
        if tutor:
            for order in self._origin.order_ids:
                spese_tecnico = 0
                commissione = 0

                tecnico_lst = self.find_comm_member(order, tecnico)
                for member in tecnico_lst:
                    if 'Spese' in member['name_commission']:
                        spese_tecnico += member['commission']
                    else:
                        commissione += member['commission']

                commission_tecnico = ((commissione/4)*3)+spese_tecnico
                commission_tutor = commissione/4

                tutor_found = False
                sales_order_commission = self.env['sales.order.commission'].search([('order_id','=',order.id)])
                for commission in sales_order_commission:
                    if commission.user_id.id == tecnico.id:
                        commission.commission = commission_tecnico
                    if commission.user_id == tutor.id:
                        tutor_found = True
                        commission.commission = commission_tutor

                if not tutor_found:
                    vals = {
                        "user_id": tutor.id,
                        "job_id": tutor.employee_id.job_id.id,
                        "commission": commission_tutor,
                        "order_id": order.id
                    }
                    self.env['sales.order.commission'].create(vals)
                    self.add_new_follower(tutor)

        else:
            self.remove_employee_comm(user_type, 142, tecnico.id)



    def add_new_follower(self, user):
        follower = self.env['mail.followers'].search(['&',('res_id','=',self._origin.id),('partner_id','=',user.partner_id.id)])
        if not follower:
            reg = {
                'res_id': self._origin.id,
                'res_model': 'crm.lead',
                'partner_id': user.partner_id.id,
            }
            try:
                self.env['mail.followers'].create(reg)
            except:
                print("Double follower")

    @api.onchange('analisi_annullata')
    def _onchange_analisi_annullata(self):
        if self.analisi_annullata:
            annullata_tag = self.env['crm.lead.tag'].search([('name','=','Annullata')]).id
            self.tag_ids = [annullata_tag]
            for opportunity in self.partner_id.opportunity_ids:
                if opportunity.team_id.name == 'Commerciali':
                    opportunity.tag_ids = [annullata_tag]
                    break

            for order in self.partner_id.sale_order_ids:
                order.state = 'cancel'
                for invoice in order.invoice_ids:
                    invoice.ref = 'no_provvigione'


    @api.onchange('tag_ids')
    def _onchange_tag_ids(self):
        self._onchange_compute_probability()
        for tag in self.tag_ids:
            if tag.name == 'In Analisi':
                sale_order = self.env['sale.order'].search([('opportunity_id','=',self._origin.id)])
                if sale_order:
                    self.for_admin = True
                break
            elif tag.name == 'Annullata':
                check_found = False
                for order in self.order_ids:
                    for line in order.order_line:
                        if "Checkup" in line.name:
                            date_check = self.date_confirm_check if self.date_confirm_check else 'DATA'
                            line.name = "Saldo Checkup preliminare autorizzato in data "+str(date_check)+". Fatturazione avvenuta come da punto D dell'incarico ricevuto."
                            check_found = True
                            break

                    if check_found:
                        break

    @api.onchange('analista')
    def _onchange_analista(self):
        if self.analista:
            self.user_id = self.analista

            thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.analista.id, 'analista'))
            thread.daemon = True                            # Daemonize thread
            thread.start()

        for opportunity in self.partner_id.opportunity_ids:
            self.update_collaborator(opportunity.order_ids, self.analista, 'analista', 137)

    @api.onchange('analista_affiancato')
    def _onchange_analista_affiancato(self):
        if self.analista_affiancato:
            thread = threading.Thread(target=self.add_participants, args=(self._origin.id, self.analista_affiancato.id, 'analista_affiancato'))
            thread.daemon = True                            # Daemonize thread
            thread.start()

        for opportunity in self.partner_id.opportunity_ids:
            self.update_collaborator_affiancato(opportunity.order_ids, self.analista, self.analista_affiancato, 'analista_affiancato', 137)

    def action_analisi_finished(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Analisi Terminata',
            'res_model': 'analisi.wizard',
            'target': 'new',
            'view_id': self.env.ref('cssa.analisi_wizard').id,
            'view_mode': 'form',
            'context': {'default_partner_id': self.partner_id.id}
        }

    def assign_leads(self):
        thread = threading.Thread(target=self.assign, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def assign(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                teleseller_3 = new_env['res.users'].search([('name','=','Telemarketing 3')])
                teleseller_4 = new_env['res.users'].search([('name','=','Telemarketing 4')])
                mail_activities = new_env['mail.activity'].search([('user_id','=',teleseller_4.id)])
                for activity in mail_activities:
                    print("mail_activity old ",activity.user_id)
                    activity.user_id = teleseller_3.id
                    print("mail_activity new ",activity.user_id)

    # def assign(self):
    #     with api.Environment.manage():
    #         with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
    #             new_env = api.Environment(new_cr, self.env.uid, self.env.context)
    #             new_teleseller = new_env['res.users'].search([('name','=','Telemarketing 3')])
    #             all_leads = new_env['crm.lead'].search(['&',('stage_id.is_won','=',False),('user_id.name','=','Telemarketing 3')])
    #             for lead in all_leads:
    #                 mail_activity = new_env['mail.activity'].search([('res_id','=',lead.id)])
    #                 print("mail_activity old ",mail_activity.user_id)
    #                 mail_activity.user_id = new_teleseller.id
    #                 print("mail_activity new ",mail_activity.user_id)
    #                 lead.teleseller = new_teleseller.id
    #                 lead.user_id = new_teleseller.id

    def migrate_leads(self):
        thread = threading.Thread(target=self.migrate, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['crm.lead'].search([])

                i = 0
                for partner in all_partners:
                    try:
                        partner.posizione_lavorativa = partner.function if partner.function else False
                    except:
                        print("No")

                    partner.partita_iva = partner.x_partita_iva if partner.x_partita_iva else ""
                    partner.motivo_firma = partner.x_motivo_firma if partner.x_motivo_firma else ""
                    partner.persone_chiave = partner.x_persone_chiave if partner.x_persone_chiave else ""
                    partner.promesse_firma = partner.x_promesse_firma if partner.x_promesse_firma else ""
                    partner.show_skype = partner.x_show_skype if partner.x_show_skype else False
                    partner.teleseller = partner.x_teleseller.id if partner.x_teleseller else False
                    partner.situazione_finanziaria = partner.x_situazione_finanziaria if partner.x_situazione_finanziaria else ""
                    partner.azienda_cliente = partner.x_azienda_cliente if partner.x_azienda_cliente else ""
                    partner.in_out = partner.x_in_out if partner.x_in_out else ""
                    partner.indicazioni_stradali = partner.x_indicazioni_stradali if partner.x_indicazioni_stradali else ""
                    partner.is_telemarketing = partner.x_is_telemarketing if partner.x_is_telemarketing else False
                    partner.categoria_interesse = partner.x_categoria_interesse if partner.x_categoria_interesse else ""
                    partner.commerciale = partner.x_commerciale.id if partner.x_commerciale else False
                    partner.for_admin = partner.x_for_admin if partner.x_for_admin else False
                    partner.ateco = partner.x_ateco if partner.x_ateco else ""
                    partner.analista = partner.x_analista.id if partner.x_analista else False
                    partner.caratteristiche_titolare = partner.x_caratteristiche_titolare if partner.x_categoria_interesse else ""
                    partner.fatturato = str(partner.x_fatturato) if partner.x_fatturato else ""
                    partner.dipendenti = str(partner.x_n_dipendenti) if partner.x_n_dipendenti else ""
                    partner.activity_orario = partner.x_activity_orario if partner.x_activity_orario else ""
                    partner.area_interesse = partner.x_area_interesse if partner.x_area_interesse else ""
                    partner.provincia = partner.x_provincia if partner.x_provincia else ""
                    partner.region = partner.x_region if partner.x_region else ""
                    partner.settore = partner.x_servizio if partner.x_servizio else ""

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_activity(self):
        thread = threading.Thread(target=self.migrate_activity_report, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_activity_report(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['crm.activity.report'].search([])

                i = 0
                for partner in all_partners:
                    try:
                        partner.azienda_cliente = partner.x_azienda_cliente if partner.x_azienda_cliente else ""
                        partner.settore = partner.x_servizio if partner.x_servizio else ""
                        partner.provincia = partner.x_provincia if partner.x_provincia else ""
                    except:
                        print("No")

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_activity_2(self):
        thread = threading.Thread(target=self.migrate_activity, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_activity(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['crm.activity.report'].search([])

                i = 0
                for partner in all_partners:
                    try:
                        partner.orario = partner.x_orario if partner.x_orario else ""
                    except:
                        print("No")

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_users(self):
        thread = threading.Thread(target=self.migrate_u, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_u(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['res.users'].search([])

                i = 0
                for partner in all_partners:
                    partner.analista = partner.x_analista.id if partner.x_analista else False
                    partner.commerciale = partner.x_commerciale.id if partner.x_commerciale else False
                    partner.teleseller = partner.x_teleseller.id if partner.x_teleseller else False

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_stages(self):
        thread = threading.Thread(target=self.migrate_s, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_s(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['crm.lead'].search([])

                i = 0
                for partner in all_partners:
                    try:
                        partner.esito = str(partner.stage_id.id)
                        print(partner.esito)
                    except:
                        print("Another stage")

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_orario(self):
        thread = threading.Thread(target=self.migrate_o, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_o(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['crm.lead'].search([])
                all_activities = new_env['mail.activity'].search([])

                i = 0
                for activity in all_activities:
                    for partner in all_partners:
                        if activity.res_id == partner.id:
                            partner.orario = activity.orario
                            break

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_azienda_cliente(self):
        thread = threading.Thread(target=self.migrate_a_c, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_a_c(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_leads = new_env['crm.lead'].search([])

                for lead in all_leads:
                    if lead.azienda_cliente:
                        lead.partner_id.azienda_cliente = lead.azienda_cliente
                        for child in lead.partner_id.child_ids:
                            child.azienda_cliente = lead.azienda_cliente

                print("Completed")


class AnalisiWizard(models.TransientModel):
    _name = 'analisi.wizard'

    partner_id = fields.Integer()
    user_email = fields.Char()
    partner_name = fields.Char()
    link_partner = fields.Char()

    importo_progetto = fields.Float('Importo Progetto')
    importo_spese = fields.Float('Importo Spese')
    go_amount = fields.Float('Importo GO')

    codice_sdi = fields.Char('Codice SDI', required=True)

    esito = fields.Selection([
        ('GO','GO'),
        ('No GO','No GO')

    ], 'Esito', required=True)

    saldo_check = fields.Selection([
        ('Assegno ricevuto','Assegno ricevuto'),
        ('Bonifico effettuato','Bonifico effettuato'),
        ('Bonifico da effettuare','Bonifico da effettuare')

    ], 'Saldo Check')

    percentuale_anticipo = fields.Selection([
        ('10%','10%'),
        ('15%','15%'),
        ('20%','20%'),
        ('25%','25%'),
        ('30%','30%'),
        ('35%','35%'),
        ('40%','40%')

    ], '% Anticipo')

    saldo_anticipo = fields.Selection([
        ('Assegno ricevuto','Assegno ricevuto'),
        ('Bonifico effettuato','Bonifico effettuato'),
        ('Bonifico da effettuare','Bonifico da effettuare')

    ], 'Saldo Anticipo')

    n_rate = fields.Selection([
        ('4','4'),
        ('6','6'),
        ('7','7'),
        ('8','8'),
        ('10','10'),
        ('12','12')

    ], 'N° Rate')

    progetto = fields.Char('Progetto')

    def action_send_analisi_result(self):
        self.go_amount = self.importo_progetto+self.importo_spese

        partner = self.sudo().env['res.partner'].search([('id','=',self.partner_id)])
        self.link_partner = 'https://mdcdigitale.cloud/web#id='+str(partner.id)+'&action=129&model=res.partner&view_type=form&cids=1&menu_id=100'
        self.partner_name = partner.name

        opportunity_analisi = False

        for opportunity in partner.opportunity_ids:
            if opportunity.team_id.name == 'Analisti':
                opportunity_analisi = opportunity
                break

        if self.esito == 'GO':
            new_stage = self.env['crm.stage'].search([('name','=','GO !!!')])

            # Set Tags
            tag_go = self.env['res.partner.category'].search([('name','=','Cliente')]).id
            opportunity_analisi.partner_id.category_id = [tag_go]

            opportunity_analisi.partner_id.customer_rank = 1

            tag_apertura_lavori = self.env['crm.lead.tag'].search([('name','=','Apertura lavori')]).id
            opportunity_analisi.tag_ids = [tag_apertura_lavori]

            for opportunity in opportunity_analisi.partner_id.opportunity_ids:
                if opportunity.team_id.name == 'Commerciali':
                    opportunity.tag_ids = [30]
                    for order in opportunity.order_ids:
                        order.esito = 'Go'
                    break

            for child in opportunity_analisi.partner_id.child_ids:
                child.category_id = [tag_go]

        else:
            self.percentuale_anticipo = ""
            self.saldo_anticipo = ""
            self.n_rate = ""

            new_stage = self.env['crm.stage'].search([('name','=','No GO')])

            # Set Tag and Esito
            for opportunity in opportunity_analisi.partner_id.opportunity_ids:
                if opportunity.team_id.name == 'Commerciali':
                    opportunity.tag_ids = [31]
                    for order in opportunity.order_ids:
                        order.esito = 'No Go'
                    break

        self.send_emails(opportunity_analisi)

        opportunity_analisi.stage_id = new_stage.id
        opportunity_analisi.saldo_check = self.saldo_check
        opportunity_analisi.percentuale_anticipo = self.percentuale_anticipo
        opportunity_analisi.saldo_anticipo = self.saldo_anticipo
        opportunity_analisi.importo_progetto = self.importo_progetto
        opportunity_analisi.importo_spese = self.importo_spese
        opportunity_analisi.n_rate = self.n_rate

    def send_emails(self, opportunity_analisi):
        commerciali_template = self.env.ref('cssa.analisi_result_commerciali_template')
        amministrazione_template = self.env.ref('cssa.analisi_result_amministrazione_template')

        if opportunity_analisi.commerciale:
            self.user_email = opportunity_analisi.commerciale.email
            commerciali_template.send_mail(self.id, force_send=True)

        if opportunity_analisi.commerciale_affiancato:
            self.user_email = opportunity_analisi.commerciale_affiancato.email
            commerciali_template.send_mail(self.id, force_send=True)

        dir_comm_group = self.sudo().env['res.groups'].search(['&',('category_id.name','=','Commerciale'),('name','=','Direttore')])
        for user in dir_comm_group.users:
            self.user_email = user.email
            commerciali_template.send_mail(self.id, force_send=True)

        amministrazione_group = self.sudo().env['res.groups'].search(['&','|',('category_id.name','=','Dipendente'),('name','=','Amministrazione'),('name','=','Back Office')])
        for user in amministrazione_group.users:
            self.user_email = user.email
            amministrazione_template.send_mail(self.id, force_send=True)





