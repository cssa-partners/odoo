# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class Zones(models.Model):
    _name = 'cssa.zones'
    _description = 'Open Zones'

    city = fields.Char(string="Città")
    provincia = fields.Char(string="Provincia")

    def find_zones(self):
        thread = threading.Thread(target=self.find_z, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def find_z(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)

                current_user = new_env.user
                activities = new_env['mail.activity'].search(['&', ('user_id', '=', current_user.id), ('date_deadline', '<', date.today())])

                for activity in activities:
                    opportunity = new_env['crm.lead'].search([('id', '=', activity.res_id)])
                    opened_zones = new_env['cssa.zones'].search([])

                    city_found = False
                    for zone in opened_zones:
                        if zone.city == opportunity.city:
                            city_found = True
                            break

                    if not city_found:
                        vals = {}
                        vals['city'] = opportunity.city
                        vals['provincia'] = opportunity.provincia

                        new_env['cssa.zones'].create(vals)


    def complete_zone(self):
        current_user = self.env.user
        opportunities = self.env['crm.lead'].search(['&',('city','=',self.city),('team_id','=',current_user.sale_team_id.id)])

        for opportunity in opportunities:
            completed_zones = self.env['cssa.completed_zones'].search([])
            activity = self.env['mail.activity'].search(['&',('res_id','=',opportunity.id),('state', '=', 'overdue')])

            if activity:
                opportunity_found = False
                for zone in completed_zones:
                    if zone.opportunity_id == opportunity.id:
                        opportunity_found = True
                        break

                if not opportunity_found:
                    activity.action_feedback()

                    vals = {}
                    vals['opportunity_id'] = opportunity.id
                    vals['opportunity_name'] = opportunity.name
                    vals['opportunity_city'] = opportunity.city
                    vals['opportunity_provincia'] = opportunity.provincia

                    self.env['cssa.completed_zones'].create(vals)
                    opportunity.write({'to_rework': True})

        self.unlink()


