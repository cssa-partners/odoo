# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Contract(models.Model):
    _inherit = 'hr.contract'

    user_name = fields.Char()
    user_email = fields.Char()

    @api.onchange('state')
    def _onchange_state(self):
        if self.state == 'close':
            print("Send email")
            self.user_name = self._origin.employee_id.name
            contract_template = self.env.ref('cssa.expired_contract_template')

            amministrazione_group = self.sudo().env['res.groups'].search(['&','|',('category_id.name','=','Dipendente'),('name','=','Amministrazione'),('name','=','Back Office')])

            for user in amministrazione_group.users:
                self.user_email = user.email
                contract_template.send_mail(self._origin.id, force_send=True)
