# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

class DataPerformance(models.Model):
    _name = 'data_performance'

    data_id = fields.Integer()
    user = fields.Char('Persona')
    ricavi = fields.Float('Ricavi')
    costi = fields.Float('Costi')
    costi_indiretti = fields.Float('Costi indiretti')
    totale = fields.Float('Totale')
    per_costi = fields.Char('% Costi')
    per_netto = fields.Char('% Netto')

class DataResocontoFinanziario(models.Model):
    _name = 'data_resoconto_finanziario'

    data_id = fields.Integer()
    tipologia = fields.Char('Tipologia')
    importo = fields.Float('Importo')

class DataResocontoEconomico(models.Model):
    _name = 'data_resoconto_economico'

    data_id = fields.Integer()
    tipologia = fields.Char('Tipologia')
    importo = fields.Float('Importo', store=True)

class ResocontoFinanziario(models.Model):
    _name = 'resoconto_finanziario'

    list = fields.One2many('data_resoconto_finanziario', 'data_id')

    def load_data(self):
        start_date = datetime.datetime(datetime.datetime.now().year, 1, 1)
        today = datetime.datetime.now()

        self.list.unlink()
        data_resoconto = self.env['data_resoconto_finanziario'].search([('data_id','>',4)])
        for d in data_resoconto:
            d.unlink()

        active_invoices = self.env['account.move'].search(['&',('date','>=',start_date),'&',('date','<=',today),'&',('invoice_payment_state','=','paid'),('journal_id.name','=','Fatture Clienti')])
        passive_invoices = self.env['account.move'].search(['&',('date','>=',start_date),'&',('date','<=',today),'&',('invoice_payment_state','=','paid'),('journal_id.name','=','Fatture Fornitore')])
        commissions = self.env['account.move'].search(['&',('date','>=',start_date),'&',('date','<=',today),'&',('invoice_payment_state','=','paid'),('journal_id.name','=','Collaboratori')])
        payslips = self.env['account.move'].search(['&',('date','>=',start_date),'&',('date','<=',today),'&',('invoice_payment_state','=','paid'),('journal_id.name','=','Personale')])
        sales_receipts = self.env['account.move'].search(['&',('date','>=',start_date),'&',('date','<=',today),('type','=','out_receipt')])
        purchase_receipts = self.env['account.move'].search(['&',('date','>=',start_date),'&',('date','<=',today),('type','=','in_receipt')])

        total_active_invoices = 0
        total_passive_invoices = 0
        total_sales_receipts = 0
        total_purchase_receipts = 0
        total_commissions = 0
        total_payslips = 0

        for ai in active_invoices:
            total_active_invoices += ai.amount_total_signed
        for pi in passive_invoices:
            total_passive_invoices += pi.amount_total_signed
        for sale in sales_receipts:
            sales_receipts += sale.amount_total_signed
        for purchase in purchase_receipts:
            total_purchase_receipts += purchase.amount_total_signed
        for c in commissions:
            total_commissions += c.amount_total_signed
        for p in payslips:
            total_payslips += p.amount_total_signed

        total_active_invoices += total_sales_receipts
        total_active_invoices = round(total_active_invoices,2)

        total_passive_invoices += total_purchase_receipts
        total_passive_invoices = round(total_passive_invoices,2)

        total_payslips = round(total_payslips,2)
        total_commissions = round(total_commissions,2)

        lines = []
        val_active_invoices = { 'data_id':5,'tipologia': 'Vendite', 'importo': total_active_invoices }
        val_passive_invoices = { 'data_id':6,'tipologia': 'Acquisti', 'importo': total_passive_invoices }
        val_commissions = { 'data_id':7,'tipologia': 'Conteggi', 'importo': total_commissions }
        val_payslips = { 'data_id':8,'tipologia': 'Paghe', 'importo': total_payslips }

        lines.append((0, 0, val_active_invoices))
        lines.append((0, 0, val_passive_invoices))
        lines.append((0, 0, val_commissions))
        lines.append((0, 0, val_payslips))

        self.list = lines

class ResocontoEconomico(models.Model):
    _name = 'resoconto_economico'

    list = fields.One2many('data_resoconto_economico', 'data_id')

    def load_data(self):
        start_date = datetime.datetime(datetime.datetime.now().year, 1, 1)

        self.list.unlink()
        data_resoconto = self.env['data_resoconto_economico'].search([('data_id','<',5)])
        for d in data_resoconto:
            d.unlink()

        active_invoices = self.env['account.move'].search(['&',('date','>=',start_date),('journal_id.name','=','Fatture Clienti')])
        passive_invoices = self.env['account.move'].search(['&',('date','>=',start_date),('journal_id.name','=','Fatture Fornitore')])
        sales_receipts = self.env['account.move'].search(['&',('date','>=',start_date),('type','=','out_receipt')])
        purchase_receipts = self.env['account.move'].search(['&',('date','>=',start_date),('type','=','in_receipt')])

        total_active_invoices = 0
        total_passive_invoices = 0
        total_sales_receipts = 0
        total_purchase_receipts = 0
        total_commissions = 0
        total_payslips = 0

        for ai in active_invoices:
            total_active_invoices += ai.amount_total_signed
        for pi in passive_invoices:
            total_passive_invoices += pi.amount_total_signed
        for sale in sales_receipts:
            sales_receipts += sale.amount_total_signed
        for purchase in purchase_receipts:
            total_purchase_receipts += purchase.amount_total_signed

        total_active_invoices += total_sales_receipts
        total_active_invoices = round(total_active_invoices,2)

        total_passive_invoices += total_purchase_receipts
        total_passive_invoices = round(total_passive_invoices,2)

        total_payslips -=self.calculate_stipendi()
        total_payslips = round(total_payslips,2)

        total_commissions += self.calculate_commissions()
        total_commissions = round(total_commissions,2)

        lines = []
        val_active_invoices = { 'data_id':1,'tipologia': 'Vendite', 'importo': total_active_invoices }
        val_passive_invoices = { 'data_id':2,'tipologia': 'Acquisti', 'importo': total_passive_invoices }
        val_commissions = { 'data_id':3,'tipologia': 'Conteggi', 'importo': total_commissions }
        val_payslips = { 'data_id':4,'tipologia': 'Paghe', 'importo': total_payslips }

        lines.append((0, 0, val_active_invoices))
        lines.append((0, 0, val_passive_invoices))
        lines.append((0, 0, val_commissions))
        lines.append((0, 0, val_payslips))

        self.list = lines

    def calculate_commissions(self):
        start_date = datetime.datetime(datetime.datetime.now().year, 1, 1)

        total_commmission = 0
        sale_orders = self.env['sale.order'].search([('date_order','>=',start_date)])

        for order in sale_orders:
            commissions = self.env['sales.order.commission'].search([('order_id','=',order.id)])
            for commission in commissions:
                total_commmission += commission.commission

        return -total_commmission

    def calculate_stipendi(self):
        total = 0

        all_users = self.env['hr.employee'].search([])
        for user in all_users:
            contract = self.env['hr.contract'].search(['&',('state','=','open'),('employee_id','=',user.id)], limit=1)

            total += (contract.wage*12)

        return total

class PerformanceCollaboratori(models.Model):
    _name = 'performance_collaboratori'

    list = fields.One2many('data_performance', 'data_id')

    def load_data(self):
        self.list.unlink()
        data_performance = self.env['data_performance'].search([])
        for d in data_performance:
            d.unlink()

        users = self.env['res.users'].search([])
        lines = []

        i = 1
        for user in users:
            vals = { 'data_id':i,'user': user.name }
            i += 1

            sale_orders = self.findSalesOrder(user)

            if len(sale_orders) > 0:
                total_sales = 0
                total_costs = 0

                for order in sale_orders:
                    total_sales += order.amount_total
                    total_costs += self.findCommissions(order)

                total_costs += self.findCosts(user)
                total_costs += self.calculate_stipendio(user)

                vals['ricavi'] = total_sales
                vals['costi'] = total_costs

                if user.sale_team_id.name == 'Commerciali':
                    vals['costi_indiretti'] = self.calculate_telemarketing_costs()
                else:
                    vals['costi_indiretti'] = 0

                vals['totale'] = vals['ricavi']-vals['costi']-vals['costi_indiretti']

                vals['per_costi'] = self.calculatePercentage(total_costs, total_sales)
                vals['per_netto'] = self.calculatePercentage(total_costs, vals['totale'])

                lines.append((0, 0, vals))

        self.list = lines

    def findCosts(self, user):
        start_date = datetime.datetime(datetime.datetime.now().year, 1, 1)
        passive_invoices = self.env['account.move'].search(['&',('date','>=',start_date),('journal_id.name','=','Fatture Fornitore')])
        total_costs = 0

        for invoice in passive_invoices:
            for line in invoice.invoice_line_ids:
                if line.product_id:
                    user_name = (user.name).split(" ")

                    if len(user_name) > 1:
                        if user_name[0] in line.product_id.name or user_name[1] in line.product_id.name:
                            total_costs += line.price_unit
                    else:
                        if user_name[0] in line.product_id.name:
                            total_costs += line.price_unit

        return total_costs

    def calculatePercentage(self, value, revenue):
        result = (100*value)/revenue
        result = round(result,2)
        return str(result)+" %"

    def findCommissions(self, order):
        total_commmission = 0

        commissions = self.env['sales.order.commission'].search([('order_id','=',order.id)])
        for commission in commissions:
            total_commmission += commission.commission

        return total_commmission

    def calculate_telemarketing_costs(self):
        users = self.env['res.users'].search([])
        telemarketing_costs = 0
        commerciali_count = 0

        for user in users:
            if user.sale_team_id.name == 'Telemarketing':
                telemarketing_costs += self.calculate_stipendio(user)
            if user.sale_team_id.name == 'Commerciali':
                commerciali_count += 1

        return telemarketing_costs/commerciali_count

    def calculate_stipendio(self, user):
        total = 0

        contract = self.env['hr.contract'].search(['&',('state','=','open'),('employee_id','=',user.id)], limit=1)
        total += (contract.wage*12)

        return total

    def findSalesOrder(self, user):
        start_date = datetime.datetime(datetime.datetime.now().year, 1, 1)
        sale_orders = []
        if user.sale_team_id.name == 'Analisti':
            sale_orders = self.env['sale.order'].search([
                '&',('date_order','>=',start_date),'|',('analista_affiancato','=',user.id),('analista','=',user.id)
            ])
        elif user.sale_team_id.name == 'Commerciali':
            sale_orders = self.env['sale.order'].search([
                '&',('date_order','>=',start_date),'|',('commerciale_affiancato','=',user.id),('commerciale','=',user.id)
            ])
        elif user.sale_team_id.name == 'Marketing':
            sale_orders = self.env['sale.order'].search([
                '&',('date_order','>=',start_date),'|',('tecnico_marketing','=',user.id),('tutor_marketing','=',user.id)
            ])
        elif user.sale_team_id.name == 'Management':
            sale_orders = self.env['sale.order'].search([
                '&',('date_order','>=',start_date),'|',('tecnico','=',user.id),('tutor','=',user.id)
            ])
        elif user.sale_team_id.name == 'Certificazioni':
            sale_orders = self.env['sale.order'].search([
                '&',('date_order','>=',start_date),'|',('tecnico_certificazioni','=',user.id),('tutor_certificazioni','=',user.id)
            ])
        elif user.sale_team_id.name == 'Digital':
            sale_orders = self.env['sale.order'].search([
                '&',('date_order','>=',start_date),'|',('tecnico_software','=',user.id),('tutor_software','=',user.id)
            ])

        return sale_orders







