# -*- coding: utf-8 -*-

from odoo import models, fields

class User(models.Model):
    _inherit = 'res.users'

    call_count = fields.Integer('Call Count')

    check_count = fields.Integer('Check Count')
    check_count_container = fields.Integer('Check Count')

    go_count = fields.Integer('Go Count')
    go_count_container = fields.Integer('Go Count')

    analista = fields.Many2one('res.users', string='Analista')
    commerciale = fields.Many2one('res.users', string='Commerciale')
    tecnico = fields.Many2one('res.users', string='Tecnico')
    teleseller = fields.Many2one('res.users', string='Teleseller')

    commission_lines = fields.One2many('commission.list', related="employee_id.slip_ids.commission_lines")
    line_ids = fields.One2many('hr.payslip.line', related="employee_id.slip_ids.line_ids")
    total_payable = fields.Float('hr.payslip', related="employee_id.slip_ids.total_payable")
    total_stock = fields.Float('hr.payslip', related="employee_id.slip_ids.total_stock")
    is_partner = fields.Boolean()

    commission_on_team = fields.Float()
