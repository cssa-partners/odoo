# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import time
import datetime
from datetime import timedelta

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class AccountPaymentTerm(models.Model):
    _inherit = 'account.payment.term'

    is_tecnico = fields.Boolean(string='Tecnico')
    has_anticipo = fields.Boolean('Contiene anticipo')

class AccountPaymentTermLine(models.Model):
    _inherit = 'account.payment.term.line'

    value_amount = fields.Float(string='Value', digits='Payment Terms', help="For percent enter a ratio between 0-100.", default=1)
