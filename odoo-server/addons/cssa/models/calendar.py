# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import datetime
from datetime import timedelta, date

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class Meeting(models.Model):
    _inherit = 'calendar.event'

    telefono = fields.Char("N° di telefono")
    indirizzo = fields.Char("Indirizzo")
    referente = fields.Char("Referente")

    @api.model
    def create(self, values):
        calendar_event = super(Meeting, self).create(values)

        if calendar_event.opportunity_id.team_id.name == 'Commerciali':
            new_opportunity = self.env['crm.lead'].search([('id','=',calendar_event.opportunity_id.id)])
            new_opportunity.date_last_appointment = calendar_event.start_datetime

        calendar_phone = ""
        calendar_street = ""
        calendar_city = ""
        calendar_provincia = ""
        calendar_zip = ""
        calendar_contact_name = ""

        if calendar_event.opportunity_id:
            calendar_phone = calendar_event.opportunity_id.phone
            calendar_street = calendar_event.opportunity_id.street
            calendar_city = calendar_event.opportunity_id.city
            calendar_provincia = calendar_event.opportunity_id.provincia
            calendar_zip = calendar_event.opportunity_id.zip
            calendar_contact_name = calendar_event.opportunity_id.contact_name
        else:
            calendar_task = self.env['project.task'].search([('id','=',calendar_event.res_id)])
            if calendar_task:
                calendar_project = self.env['project.project'].search([('id','=',calendar_task.project_id.id)])
                calendar_partner = calendar_project.partner_id
            else:
                calendar_partner = self.env['res.partner'].search([('id','=',calendar_event.res_id)])

            calendar_phone = calendar_partner.phone
            calendar_street = calendar_partner.street
            calendar_city = calendar_partner.city
            calendar_provincia = calendar_partner.provincia
            calendar_zip = calendar_partner.zip
            if len(calendar_partner.child_ids) > 0:
                calendar_contact_name = calendar_partner.child_ids[0].name


        if calendar_event.opportunity_id or calendar_partner:
            calendar_event.telefono = calendar_phone
            calendar_event.indirizzo = calendar_street
            calendar_event.indirizzo = calendar_event.indirizzo + ", " + calendar_city
            calendar_event.indirizzo = calendar_event.indirizzo + ", " + calendar_zip
            calendar_event.indirizzo = calendar_event.indirizzo + " (" + calendar_provincia + ")"
            calendar_event.referente = calendar_contact_name

            calendar_event.name += " - " + calendar_city + " (" + calendar_provincia + ")"

        return calendar_event

    def write(self, values):
        current_commerciale = self.opportunity_id.commerciale
        current_commerciale_affiancato = self.opportunity_id.commerciale_affiancato
        current_user = self.env.user

        is_amministrazione = False
        is_cliente = False

        for user_group in current_user.groups_id:
            if user_group.name == 'Cliente':
                is_cliente = True
            if user_group.category_id.name == 'Dipendente' and (user_group.name == 'Amministrazione' or user_group.name == 'Back Office'):
                is_amministrazione = True

        if not is_cliente:
            if not is_amministrazione and current_commerciale:
                values['user_id'] = current_commerciale.id
                new_partner_id = self.env['res.partner'].search([('email', '=', current_commerciale.email)]).id

                if current_commerciale_affiancato:
                    new_partner_affiancato_id = self.env['res.partner'].search([('email', '=', current_commerciale_affiancato.email)]).id
                    values['partner_ids'] = [new_partner_id, new_partner_affiancato_id]
                else:
                    values['partner_ids'] = [new_partner_id]

                values['duration'] = 1.5
                start = datetime.datetime.strptime(self.start.strftime(DEFAULT_SERVER_DATETIME_FORMAT), DEFAULT_SERVER_DATETIME_FORMAT)
                values['stop'] = start + timedelta(hours=1.5)

            elif is_amministrazione and current_commerciale:
                #new_partner_id = self.env['res.partner'].search([('email', '=', current_commerciale.email)]).id
                amministrazione_group = self.env['res.groups'].search(['&','|',('category_id.name','=','Dipendente'),('name','=','Amministrazione'),('name','=','Back Office')])

                if amministrazione_group in self.opportunity_id.analista.groups_id:
                    programma_analisi_id = self.env['res.partner'].search([('name', '=', 'PROGRAMMA ANALISI')]).id
                    #self.update_date_analisi(self.opportunity_id.partner_id, False)
                    values['partner_ids'] = [programma_analisi_id]

                elif amministrazione_group in self.opportunity_id.analista.groups_id and self.opportunity_id.analista.id != self.opportunity_id.user_id.id:
                    new_partner_id = self.env['res.partner'].search([('email', '=', self.env.user.email)]).id
                    sale_partner_id = self.env['res.partner'].search([('email', '=', self.opportunity_id.user_id.email)]).id
                    values['partner_ids'] = [new_partner_id, sale_partner_id]
                    values['user_id'] = self.opportunity_id.user_id.id

                elif self.opportunity_id.analista_affiancato:
                    analista_affiancato_id = self.env['res.partner'].search([('email', '=', self.opportunity_id.analista_affiancato.email)]).id
                    values['partner_ids'] = [analista_affiancato_id]
        else:
            values['user_id'] = self.opportunity_id.user_id.id


        super(Meeting, self).write(values)

    def unlink(self, can_be_deleted=True):
        #self.update_date_analisi(self.opportunity_id.partner_id, True)
        super(Meeting, self).unlink()

    def update_date_analisi(self, partner, empty):
        for opportunity in partner.opportunity_ids:
            if opportunity.team_id.name == 'Commerciali' or opportunity.team_id.name == 'Analisti':
                if empty:
                    opportunity.date_analisi = False
                    for order in opportunity.order_ids:
                        order.date_analisi = False
                else:
                    opportunity.date_analisi = self.start_datetime
                    for order in opportunity.order_ids:
                        order.date_analisi = self.start_datetime
                #break


    @api.depends('allday', 'start', 'stop')
    def _compute_dates(self):
        """ Adapt the value of start_date(time)/stop_date(time) according to start/stop fields and allday. Also, compute
            the duration for not allday meeting ; otherwise the duration is set to zero, since the meeting last all the day.
        """
        for meeting in self:
            if meeting.allday and meeting.start and meeting.stop:
                meeting.start_date = meeting.start.date()
                meeting.start_datetime = False
                meeting.stop_date = meeting.stop.date()
                meeting.stop_datetime = False

                meeting.duration = 0.0
            else:
                meeting.start_date = False
                meeting.start_datetime = meeting.start
                meeting.stop_date = False
                meeting.stop_datetime = meeting.stop

                meeting.duration = 1.50


    @api.onchange('user_id')
    def _onchange_user_id(self):
        current_user = self.env.user
        is_cliente = False
        for group in current_user.groups_id:
            if group.name == 'Cliente':
                is_cliente = True
                break

        if is_cliente:
            users = self.env['res.users'].search([('groups_id.name','=','Cliente')])
            return {'domain': {'user_id': [('id', 'in', users.ids)]}}


    def new_spazio_analisi(self):
        spazio_analisi_user = self.env['res.users'].search([('name','=','SPAZIO ANALISI')])
        today_events = self.env['calendar.event'].search([('user_id','=',spazio_analisi_user.id)])

        spazio_analisi_counter = 0
        for event in today_events:
            if(event.start.date() == self.start.date()):
                spazio_analisi_counter += 1

        current_company = self.env['res.company'].search([('id','=',self.env.user.company_id.id)])
        if spazio_analisi_counter < current_company.n_spazio_analisi:
            self.user_id = spazio_analisi_user.id

            spazio_analisi_group = self.env['res.groups'].search(['&',('category_id.name','=','Analisi'),('name','=','Spazio analisi')])
            spazio_analisi_users = []

            for group_user in spazio_analisi_group.users:
                spazio_analisi_users.append(group_user.partner_id.id)

            self.partner_ids = spazio_analisi_users

    def migrate_calendar(self):
        thread = threading.Thread(target=self.migrate, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_partners = new_env['calendar.event'].search([])

                i = 0
                for partner in all_partners:
                    try:
                        partner.indirizzo = partner.x_indirizzo if partner.x_indirizzo else ""
                        partner.telefono = partner.x_telefono if partner.x_telefono else ""
                        partner.referente = partner.x_referente if partner.x_referente else ""
                        print('wooo')
                    except:
                        print("No")

                    print(str(i))
                    i = i + 1
                print("Completed")

    def migrate_last_appointment(self):
        thread = threading.Thread(target=self.migrate_last_app, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_last_app(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                calendar_events = new_env['calendar.event'].search([])

                for event in calendar_events:
                    if event.opportunity_id.team_id.name == 'Commerciali':
                        event.opportunity_id.date_last_appointment = event.start_datetime


                print("Completed")
