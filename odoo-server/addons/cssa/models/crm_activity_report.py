# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ActivityReport(models.Model):
    _inherit = 'crm.activity.report'

    azienda_cliente = fields.Char(related='partner_id.azienda_cliente', readonly=True)
    settore = fields.Char(related='partner_id.settore', readonly=True)
    provincia = fields.Char(related='partner_id.provincia', readonly=True)

