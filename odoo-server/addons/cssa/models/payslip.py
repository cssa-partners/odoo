# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date, datetime, timedelta
import requests
import urllib.parse

class KmList(models.Model):
    _name = 'km.list'

    payslip_id = fields.Many2one('hr.payslip', string='Payslip ID')

    date = fields.Datetime('Data')
    name = fields.Char('Nome')
    origin = fields.Char('Origine')
    destination = fields.Char('Destinazione')
    km = fields.Float('Km')

class CommissionList(models.Model):
    _name = 'commission.list'

    payslip_id = fields.Many2one('hr.payslip', string='Payslip ID')
    partner_name = fields.Char('Nome Azienda')
    state = fields.Char('Stato')

    total_go = fields.Float('Total Go')
    per_go = fields.Float('Percentuale su Go')
    check_flag = fields.Float('Check')

    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    saldo = fields.Float('Saldo')


class ProductList(models.Model):
    _name = 'product.list'

    payslip_id = fields.Many2one('hr.payslip', string='Payslip ID')
    product_id = fields.Integer()
    name = fields.Char()
    amount = fields.Float()
    uom = fields.Integer()

class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'

    def write(self, vals):
        payslip = self.env['hr.payslip'].search([('id','=',self.slip_id.id)])
        payslip.calculateExtra(self)

        super(HrPayslipLine, self).write(vals)

class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    commission_lines = fields.One2many('commission.list', 'payslip_id', string='Lista Provvigioni')
    km_lines = fields.One2many('km.list', 'payslip_id', string='Kilometri percorsi')

    products = fields.One2many('product.list', 'payslip_id', string='Lista Prodotti')

    total_payable = fields.Float('Totale a ricevere')
    total_stock = fields.Float('Totale magazzino')

    bonus_check = fields.Integer('Bonus Check')
    bonus_go = fields.Integer('Bonus Go')

    total_km = fields.Float('Km totali')

    def compute_sheet(self):
        # OVERRIDE
        self.calculateBonus(self.employee_id.user_id)

        if self.employee_id.department_id.id == 5:
            self.manageDynamicPercentage()

        super(HrPayslip, self).compute_sheet()

        user_payslip = self.env['hr.payslip'].search([('employee_id','=',self.employee_id.id)])
        user_payslip.line_ids.unlink()

        for payslip in self:
            contract_ids = payslip.contract_id.ids or \
                           self.get_contract(payslip.employee_id, payslip.date_from, payslip.date_to)
            lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]

        self.employee_id.slip_ids.line_ids = lines

        self.products = False

        self.total_payable = 0
        self.calculateExtra(self._get_payslip_lines(contract_ids, payslip.id))
        self.calculateCommissions()

        #self.calculate_journey()

        return True

    def calculateBonus(self, user):
        if user.sale_team_id.id == 8:
            won_opportunities = self.env['crm.lead'].search(['&','&','&',('user_id','=',user.id),('date_last_stage_update','>=',self.date_from),('date_last_stage_update','<=',self.date_to),('stage_id.is_won','=',True)])

            if self.employee_id.n_min_appointments:
                n_min_appointments = self.employee_id.n_min_appointments*self.worked_days_line_ids[0].number_of_days
            else:
                n_min_appointments = 0

            in_counter = 0
            for opportunity in won_opportunities:
                for opportunity_commerciale in opportunity.partner_id.opportunity_ids:
                    if self.date_from <= opportunity_commerciale.date_last_stage_update.date() <= self.date_to and opportunity_commerciale.team_id.id == 9 and opportunity_commerciale.in_out == 'IN':
                        in_counter += 1

            per_in = 0
            if len(won_opportunities) > 0:
                per_in = (100*in_counter)/len(won_opportunities)

            if len(won_opportunities)-n_min_appointments >= 0:
                extra_appointments = len(won_opportunities)-n_min_appointments
                user.call_count = (extra_appointments*per_in)/100
        else:
            now = datetime.now()
            start_year = datetime(now.year, 1, 1)
            won_opportunities = self.env['crm.lead'].search(['&','&','&',('commerciale','=',user.id),('date_last_stage_update','>=',start_year),('date_last_stage_update','<=',self.date_to),('stage_id.is_won','=',True)]).sorted(key=lambda r: r.date_last_stage_update)
            check_counter = 0
            go_counter = 0

            for opportunity in won_opportunities:
                if opportunity.team_id.id == 9:
                    for order in opportunity.order_ids:
                        if order.invoice_count > 0:
                            check_counter += 1
                            break
                elif opportunity.team_id.id == 10:
                    go_counter += 1

            #user.check_count = len(won_opportunities)-user.check_count_container
            if check_counter-user.check_count_container >= 0:
                user.check_count = check_counter-user.check_count_container
            if go_counter-user.go_count_container >= 0:
                user.go_count = go_counter-user.go_count_container

        self.bonus_check = user.check_count
        self.bonus_go = user.go_count


    def manageDynamicPercentage(self):
        won_opportunities = self.env['crm.lead'].search(['&','&','&',('user_id','=',self.employee_id.user_id.id),('date_last_stage_update','>=',self.date_from),('date_last_stage_update','<=',self.date_to),('stage_id.is_won','=',True)])

        if len(won_opportunities) > self.employee_id.soglia_check and self.employee_id.variazione_provvigione > 0:
            for opportunity in won_opportunities[self.employee_id.soglia_check:]:
                for order in opportunity.order_ids:
                    sales_order_commission = self.env['sales.order.commission'].search(['&',('user_id','=',self.employee_id.user_id.id),('order_id','=',order.id)])
                    member_lst = self.find_comm_member(order, self.employee_id.user_id, 0, self.employee_id.variazione_provvigione)
                    for member in member_lst:
                        sales_order_commission.commission = member['commission']

    def resetBonus(self):
        self.employee_id.user_id.check_count = 0
        self.employee_id.user_id.go_count = 0
        self.bonus_check = 0
        self.bonus_go = 0

    def findAnticipoAmount(self, order):
        anticipo_amount = 0
        commission = 0

        for line in order.order_line:
            if line.qty_invoiced <= 0:
                continue

            if 'Anticipo' in line.product_id.name:
                anticipo_amount = line.price_unit
                break

        if anticipo_amount > 0:
            member_lst = self.find_comm_member(order, self.employee_id.user_id, anticipo_amount, 0)
            for member in member_lst:
                if 'Check' not in member['name_commission']:
                    commission += self.manageCommission(member)

        return commission

    def hasTutor(self, user, opportunity):
        has_tutor = False
        if opportunity.tutor and user.id == opportunity.tecnico.id:
            has_tutor = True
        elif opportunity.tutor_certificazioni and user.id == opportunity.tecnico_certificazioni.id:
            has_tutor = True
        elif opportunity.tutor_marketing and user.id == opportunity.tecnico_marketing.id:
            has_tutor = True
        elif opportunity.tutor_software and user.id == opportunity.tecnico_software.id:
            has_tutor = True

        return has_tutor

    def isTutor(self, user, opportunity):
        is_tutor = False
        if user.id == opportunity.tutor.id:
            is_tutor = True
        elif user.id == opportunity.tutor_certificazioni.id:
            is_tutor = True
        elif user.id == opportunity.tutor_marketing.id:
            is_tutor = True
        elif user.id == opportunity.tutor_software.id:
            is_tutor = True

        return is_tutor

    def findTecnicoPaymentTerms(self, opportunity):
        if opportunity.tecnico and opportunity.tecnico.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tecnico_payment_term_id
        elif opportunity.tutor and opportunity.tutor.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tutor_payment_term_id
        elif opportunity.tecnico_certificazioni and opportunity.tecnico_certificazioni.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tecnico_certificazioni_payment_term_id
        elif opportunity.tutor_certificazioni and opportunity.tutor_certificazioni.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tutor_certificazioni_payment_term_id
        elif opportunity.tecnico_marketing and opportunity.tecnico_marketing.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tecnico_marketing_payment_term_id
        elif opportunity.tutor_marketing and opportunity.tutor_marketing.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tutor_marketing_payment_term_id
        elif opportunity.tecnico_software and opportunity.tecnico_software.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tecnico_software_payment_term_id
        elif opportunity.tutor_software and opportunity.tutor_software.id == self.employee_id.user_id.id:
            payment_terms = opportunity.tutor_software_payment_term_id

        return payment_terms.line_ids

    def findTecnicoCommission(self, order, opportunity):
        commission_month = 0
        commission = 0
        full_commission = 0
        spese_tecnico = 0
        ref_date = order.start_contract_date

        member_lst = self.find_comm_member(order, self.employee_id.user_id, 0, 0)

        if order.assignment_amout > 0 and self.employee_id.user_id.id == order.riassigned_user.id:
            commission = order.assignment_amout
            ref_date = order.assignment_date
        else:
            for member in member_lst:
                if 'Check' not in member['name_commission'] and 'Spese' not in member['name_commission']:
                    commission += self.manageCommission(member)

                if 'Spese' in member['name_commission']:
                    spese_tecnico += member['commission']

        per_go = round(commission, 2)

        if self.hasTutor(self.employee_id.user_id, order.opportunity_id):
            full_commission = ((commission/4)*3)+spese_tecnico
            commission = full_commission
        elif self.isTutor(self.employee_id.user_id, order.opportunity_id):
            full_commission = commission/4
            commission = full_commission

        if ref_date:
            today = (self.date_from).strftime("%Y-%m-%d")
            days_from_contract = self.days_between(today, str(ref_date))

            terms_expired = True
            for line in self.findTecnicoPaymentTerms(opportunity):
                if days_from_contract < line.days or line.days == 0:
                    if line.value_amount == 1:
                        continue

                    commission = (commission*line.value_amount)/100
                    terms_expired = False

                    months_n = ((line.days)/10)/3

                    if months_n == 1:
                        commission_month = ref_date.month
                    else:
                        if (ref_date.month+months_n) <= 12:
                            commission_month = ref_date.month+months_n
                        else:
                            commission_month = (ref_date.month+months_n)-12
                    break

            if terms_expired:
                commission = 0

        for member in member_lst:
            if ref_date and self.date_from <= ref_date <= self.date_to:
                if 'Spese' in member['name_commission']:
                    self.managePurchaseOrder(member['name_commission'], member['code_commission'], order.partner_id.name, spese_tecnico)
                else:
                    self.managePurchaseOrder(member['name_commission'], member['code_commission'], order.partner_id.name, commission)

        commission += spese_tecnico
        return [commission, commission_month, per_go, full_commission]

    def findSaldoTecnico(self, order, full_commission):
        saldo = full_commission
        today = (self.date_from).strftime("%Y-%m-%d")

        ref_date = order.start_contract_date
        if order.assignment_amout > 0 and self.employee_id.user_id.id == order.riassigned_user.id:
            ref_date = order.assignment_date

        days_from_contract = self.days_between(today, str(ref_date))

        if 30 < days_from_contract < 60:
            saldo = (full_commission*70)/100
        elif 60 < days_from_contract < 90:
            saldo = full_commission/2
        elif days_from_contract >= 90:
            saldo = (full_commission*30)/100

        self.total_stock += saldo

        return saldo

    def findBaseCommission(self, order):
        user_commission = self.env['sales.order.commission'].search(['&',('order_id','=',order.id), ('user_id','=',self.employee_id.user_id.id)], limit=1)
        commission_amount = user_commission.commission
        total_commission = user_commission.commission
        anticipo_amount = 0

        if not self.employee_id.user_id.is_partner:
            anticipo_amount = self.findAnticipoAmount(order)
            if order.opportunity_id.commerciale_affiancato or order.opportunity_id.analista_affiancato:
                anticipo_amount /= 2

            commission_amount -= anticipo_amount

        if len(order.installment_lines) > 1:
            commission_amount /= (len(order.installment_lines)-1)

        commission_amount = round(commission_amount,2)

        return [commission_amount,total_commission,anticipo_amount]

    def calculateProjectAdvance(self, order, paid_invoices_result, dictionary):
        anticipo_amount = 0
        project_anticipo = self.env['project.anticipo'].search(['&',('user_id','=',self.employee_id.user_id.id),('partner_id','=',order.partner_id.id)])

        if project_anticipo:
            for anticipo in project_anticipo:
                anticipo_amount += anticipo.anticipo

            self.total_stock -= anticipo_amount
            self.total_payable -= anticipo_amount

            anticipo_commission = anticipo_amount/(12-len(paid_invoices_result))
            anticipo_commission = round(anticipo_commission, 2)

            for x in range(1,13):
                month_found = False
                for month in paid_invoices_result:
                    if month == x:
                        month_found = True
                        break

                if not month_found and dictionary['month_'+str(x)] > 0:
                    dictionary['month_'+str(x)] -= anticipo_commission

            dictionary['saldo'] -= anticipo_amount
            for pro in project_anticipo:
                dictionary['month_'+str(pro.date_anticipo.month)] += anticipo_amount

        return [dictionary,project_anticipo]

    def findEmployeeOpportunities(self, tecnico_group, commerciale_group, analista_group):
        employee_opportunities = []

        if analista_group in self.employee_id.user_id.groups_id:
            employee_opportunities = self.env['crm.lead'].search(['&',('team_id','=',10),'|',('analista','=',self.employee_id.user_id.id),('analista_affiancato','=',self.employee_id.user_id.id)])

        if commerciale_group in self.employee_id.user_id.groups_id:
            employee_opportunities = self.env['crm.lead'].search(['&',('team_id','=',9),'|',('commerciale','=',self.employee_id.user_id.id),('commerciale_affiancato','=',self.employee_id.user_id.id)])

        if tecnico_group in self.employee_id.user_id.groups_id:
            employee_opportunities = self.env['crm.lead'].search([
                '|','|','|','|','|','|','|','|','|','|','|',
                ('tecnico','=',self.employee_id.user_id.id),
                ('tutor','=',self.employee_id.user_id.id),
                ('tecnico_marketing','=',self.employee_id.user_id.id),
                ('tutor_marketing','=',self.employee_id.user_id.id),
                ('tecnico_certificazioni','=',self.employee_id.user_id.id),
                ('tutor_certificazioni','=',self.employee_id.user_id.id),
                ('tecnico_software','=',self.employee_id.user_id.id),
                ('tutor_software','=',self.employee_id.user_id.id),
                ('partner_1','=',self.employee_id.user_id.id),
                ('partner_2','=',self.employee_id.user_id.id),
                ('partner_3','=',self.employee_id.user_id.id),
                ('partner_4','=',self.employee_id.user_id.id)
            ])

        return employee_opportunities


    def setUpPaymentsByMonth(self, dictionary, month, commission):
        if month == 1:
            dictionary['month_1'] += commission
        elif month == 2:
            dictionary['month_2'] += commission
        elif month == 3:
            dictionary['month_3'] += commission
        elif month == 4:
            dictionary['month_4'] += commission
        elif month == 5:
            dictionary['month_5'] += commission
        elif month == 6:
            dictionary['month_6'] += commission
        elif month == 7:
            dictionary['month_7'] += commission
        elif month == 8:
            dictionary['month_8'] += commission
        if month == 9:
            dictionary['month_9'] += commission
        elif month == 10:
            dictionary['month_10'] += commission
        elif month == 11:
            dictionary['month_11'] += commission
        elif month == 12:
            dictionary['month_12'] += commission

        return dictionary

    def calculatePaidInvoices(self, order, dictionary, commission_amount):
        is_invoice_anticipo = False
        paid_months = []

        for invoice in order.invoice_ids:
            if invoice.ref == 'no_provvigione' or invoice.type != 'out_invoice':
                continue

            if invoice.anticipo:
                is_invoice_anticipo = True

            invoice_payment = self.env['account.payment'].search(['&',('partner_id','!=',False),('invoice_ids','in',invoice.id)])

            for payment in invoice_payment:
                payment_amount = self.findAnticipoAmount(order) if is_invoice_anticipo else commission_amount

                if is_invoice_anticipo and (order.opportunity_id.commerciale_affiancato or order.opportunity_id.analista_affiancato):
                    payment_amount /= 2

                if payment.move_reconciled:
                    if payment.payment_date <= self.date_to:
                        if self.date_to.year == payment.payment_date.year:
                            paid_months.append(int(payment.payment_date.month))

                        #print("Pagamento:", order.partner_id.name,payment_amount,payment.name)
                        if dictionary['saldo'] > 0:
                            dictionary['saldo'] -= payment_amount
                        if self.total_stock > 0:
                            self.total_stock -= payment_amount

                    if self.date_from <= payment.payment_date <= self.date_to:
                        self.total_payable += payment_amount
                        member_lst = self.find_comm_member(order, self.employee_id.user_id, 0, 0)
                        print("member_lst",member_lst)
                        for member in member_lst:
                            print("Pagamento:", order.partner_id.name,payment_amount,payment.name)
                            self.managePurchaseOrder(member['name_commission'], member['code_commission'], order.partner_id.name, payment_amount)

                is_invoice_anticipo = False

        return [dictionary,paid_months]

    def managePurchaseOrder(self, commission_name, code_commission, partner_name, commission_amount):
        products = []

        project_anticipo = self.env['project.anticipo'].search(['&',('user_id','=',self.employee_id.user_id.id),('partner_id.name','=',partner_name)])
        if project_anticipo:
            for anticipo in project_anticipo:
                if anticipo.anticipo > 0:
                    commission_amount -= anticipo.anticipo

        if len(self.products) == 0:
            products = self.addProductToOrder(partner_name, commission_name, code_commission, commission_amount, products)
        else:
            for product in self.products:
                val = {
                    'payslip_id': product.payslip_id,
                    'product_id': product.product_id,
                    'name': product.name,
                    'amount': product.amount,
                    'uom': product.uom
                }
                products.append((0,0,val))
            self.products = False
            products = self.addProductToOrder(partner_name, commission_name, code_commission, commission_amount, products)

        self.products = products

    def addProductToOrder(self, partner_name, commission_name, code_commission, commission_amount, products):
        product_to_add = self.env['product.product'].search(['&',('default_code','=',code_commission),('name','=',commission_name)], limit=1)
        print("product ot ", product_to_add.name,commission_name,commission_amount)
        if product_to_add and commission_amount > 0:
            val = {
                'payslip_id': self._origin.id,
                'product_id': product_to_add.id,
                'name': partner_name,
                'amount': commission_amount,
                'uom': product_to_add.uom_id.id
            }
            products.append((0,0,val))
        return products

    def manageOrderState(self, order, dictionary):
        if order.state == 'cancel':
            dictionary['state'] = 'Annullato'
        elif order.state == 'draft':
            dictionary['state'] = 'Preventivo'
        else:
            dictionary['state'] = 'Confermato'

        return dictionary

    def calculateCommissions(self):
        self.commission_lines = False
        self.total_stock = 0

        tecnico_group = self.env['res.groups'].search(['&',('category_id.name','=','Tecnico'),('name','=','User')])
        commerciale_group = self.env['res.groups'].search(['&',('category_id.name','=','Commerciale'),('name','=','User')])
        analista_group = self.env['res.groups'].search(['&',('category_id.name','=','Analisi'),('name','=','User')])

        employee_opportunities = self.findEmployeeOpportunities(tecnico_group, commerciale_group, analista_group)

        commissions_dict = []
        for opportunity in employee_opportunities:
            dictionary = {
                'opportunity_name': opportunity.partner_id.name,
                'check_flag': 0,
                'total_go': 0,
                'per_go': 0,
                'month_1': 0,
                'month_2': 0,
                'month_3': 0,
                'month_4': 0,
                'month_5': 0,
                'month_6': 0,
                'month_7': 0,
                'month_8': 0,
                'month_9': 0,
                'month_10': 0,
                'month_11': 0,
                'month_12': 0,
                'saldo': 0,
                'state': ''
            }

            for order in opportunity.partner_id.sale_order_ids:
                total_contract = 0
                is_check = False

                for line in order.order_line:
                    if 'Check' in line.product_id.name:
                        is_check = True

                    if 'Anticipo' not in line.product_id.name and 'Check' not in line.product_id.name:
                        if ('Spese' in line.product_id.name and tecnico_group in self.employee_id.user_id.groups_id) or 'Spese' not in line.product_id.name:
                            total_contract += line.price_unit

                if total_contract > 0:
                    dictionary['total_go'] = total_contract

                if tecnico_group in self.employee_id.user_id.groups_id and not self.employee_id.user_id.is_partner:
                    if order.state != 'sale':
                        continue

                    dictionary = self.manageOrderState(order, dictionary)

                    if dictionary['state'] == 'Confermato':
                        tecnico_values = self.findTecnicoCommission(order, opportunity)
                        commission = tecnico_values[0]

                        full_commission = tecnico_values[3]

                        commission = round(commission,2)

                        commission_month = tecnico_values[1]

                        #print("commission",commission)
                        #print("commission_month",commission_month)
                        dictionary = self.setUpPaymentsByMonth(dictionary, commission_month, commission)
                        dictionary['per_go'] = tecnico_values[2]
                        dictionary['saldo'] += self.findSaldoTecnico(order, full_commission)

                        self.total_payable += commission

                        for tag in opportunity.tag_ids:
                            if tag.name == 'Lavoro Completato':
                                dictionary['saldo'] = 0
                                break

                else:
                    commission_values = self.findBaseCommission(order)
                    commission = commission_values[0]
                    anticipo = commission_values[2]
                    dictionary = self.manageOrderState(order, dictionary)

                    if not is_check:
                        dictionary['per_go'] = commission_values[1]

                    if dictionary['state'] == 'Confermato':

                        if len(order.installment_lines) == 1:
                            if analista_group in self.employee_id.user_id.groups_id:
                                if self.date_from.year <= order.installment_lines[0].due_date.year <= self.date_to.year:
                                    dictionary = self.setUpPaymentsByMonth(dictionary, order.installment_lines[0].due_date.month, commission)
                            elif order.start_contract_date and self.date_from.year <= order.start_contract_date.year <= self.date_to.year:
                                dictionary = self.setUpPaymentsByMonth(dictionary, order.start_contract_date.month, commission)
                        else:
                            i = 0 if len(order.installment_lines) == 1 else 1
                            for i in range(len(order.installment_lines)):
                                value_amount = anticipo if i == 0 else commission
                                if self.date_from.year <= order.installment_lines[i].due_date.year <= self.date_to.year:
                                    dictionary = self.setUpPaymentsByMonth(dictionary, order.installment_lines[i].due_date.month, value_amount)

                        if not self.employee_id.user_id.is_partner:
                            self.total_stock += commission_values[1]
                            dictionary['saldo'] += commission_values[1]
                        else:
                            self.total_payable += commission

                    if is_check:
                        dictionary['check_flag'] = commission

                    if not self.employee_id.user_id.is_partner:
                        paid_invoices_result = self.calculatePaidInvoices(order, dictionary, commission)
                        dictionary = paid_invoices_result[0]

                        if not is_check and len(order.installment_lines) > 1:
                            advance_results = self.calculateProjectAdvance(order, paid_invoices_result[1], dictionary)
                            dictionary = advance_results[0]
                            for product in self.products:
                                for pro in advance_results[1]:
                                    if product.name == advance_results[1].partner_id.name and pro.date_anticipo == self.date_to:
                                        product.amount = dictionary['month_'+str(pro.date_anticipo.month)]
                                        self.total_payable += dictionary['month_'+str(pro.date_anticipo.month)]

                    else:
                        member_lst = self.find_comm_member(order, self.employee_id.user_id, 0, 0)
                        for member in member_lst:
                            self.managePurchaseOrder(member['name_commission'], member['code_commission'], order.partner_id.name, commission)

            dictionary['saldo'] = round(dictionary['saldo'],2)
            if len(opportunity.partner_id.sale_order_ids) > 0 and dictionary['state'] != '':
                if dictionary['state'] == 'Confermato':
                    if dictionary['month_1'] > 0 or dictionary['month_2'] > 0 or dictionary['month_3'] > 0 or dictionary['month_4'] > 0 or dictionary['month_5'] > 0 or dictionary['month_6'] > 0 or dictionary['month_7'] > 0 or dictionary['month_8'] > 0 or dictionary['month_9'] > 0 or dictionary['month_10'] > 0 or dictionary['month_11'] > 0 or dictionary['month_12'] > 0 or dictionary['saldo'] > 0:
                        commissions_dict.append(dictionary)
                elif dictionary['saldo'] > 0:
                    commissions_dict.append(dictionary)

        lines = []
        for dic in commissions_dict:
            vals = {'payslip_id': self._origin.id, 'partner_name': dic['opportunity_name'], 'total_go': dic['total_go'], 'per_go': dic['per_go'],
                    'check_flag': dic['check_flag'], 'month_1': dic['month_1'], 'month_2': dic['month_2'], 'month_3': dic['month_3'],
                    'month_4': dic['month_4'], 'month_5': dic['month_5'], 'month_6': dic['month_6'], 'month_7': dic['month_7'],
                    'month_8': dic['month_8'], 'month_9': dic['month_9'], 'month_10': dic['month_10'], 'month_11': dic['month_11'],
                    'month_12': dic['month_12'], 'saldo': dic['saldo'], 'state': dic['state']}

            lines.append((0, 0, vals))

        self.commission_lines = lines

        self.employee_id.slip_ids.commission_lines = False
        self.employee_id.slip_ids.commission_lines = lines

        self.employee_id.slip_ids.total_payable = self.total_payable
        self.employee_id.slip_ids.total_stock = self.total_stock

        self.total_stock = round(self.total_stock,0)
        self.total_payable = round(self.total_payable,0)


    def manageCommission(self, member):
        commission = member['commission']
        commission = round(commission,2)

        return  commission

    def calculateExtra(self, lines):
        products = []
        fisso_found = False
        rimborso_found = False

        for slip in lines:
            if slip['code'] != 'SALEC':
                self.total_payable += float(slip['amount'])

            if slip['category_id'] == 1 and not fisso_found:
                fisso_product = self.env['product.product'].search([('name','=','Fisso')])
                val = {
                    'payslip_id': self._origin.id,
                    'product_id': fisso_product.id,
                    'name': fisso_product.name,
                    'amount': float(slip['amount']),
                    'uom': fisso_product.uom_id.id
                }
                products.append((0,0,val))
                fisso_found = True

            elif slip['category_id'] == 18 and not rimborso_found:
                rimborso_product = self.env['product.product'].search([('name','=','Rimborso Spese')])
                val = {
                    'payslip_id': self._origin.id,
                    'product_id': rimborso_product.id,
                    'name': rimborso_product.name,
                    'amount': float(slip['amount']),
                    'uom': rimborso_product.uom_id.id
                }
                products.append((0,0,val))
                rimborso_found = True

        self.products = products


    def find_comm_member(self, order, user, amount, percentage):
        member_lst = []
        emp_id = self.env['hr.employee'].search([('user_id', '=', user.id)], limit=1)

        for soline in order.order_line:
            for lineid in soline.product_id.product_comm_ids:
                commission = lineid.commission + percentage
                amount_total = amount if amount else soline.price_subtotal
                lines_user = {'user_id': user.id, 'job_id': emp_id.job_id.id, 'name_commission': lineid.product_id.name, 'code_commission': lineid.product_id.default_code}

                if lineid.user_ids and user.id in [user.id for user in lineid.user_ids]:
                    lines_user[
                        'commission'] = amount_total * commission / 100 if lineid.compute_price_type == 'per' else commission * soline.product_uom_qty
                    member_lst.append(lines_user)
                elif lineid.job_id and not lineid.user_ids:
                    if user.id in self.job_related_users(lineid.job_id):
                        lines_user[
                            'commission'] = amount_total * commission / 100 if lineid.compute_price_type == 'per' else commission * soline.product_uom_qty
                        member_lst.append(lines_user)

        return member_lst

    def job_related_users(self, jobid):
        if jobid:
            empids = self.env['hr.employee'].search([('user_id', '!=', False), ('job_id', '=', jobid.id)])
            return [emp.user_id.id for emp in empids]
        return False

    def days_between(self, d1, d2):
        try:
            d1 = datetime.strptime(d1, "%Y-%m-%d")
            d2 = datetime.strptime(d2, "%Y-%m-%d")
            return abs((d2 - d1).days)
        except:
            return 0


    def newPayment(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Pagamento',
            'res_model': 'pay.salary.wizard',
            'target': 'new',
            'view_id': self.env.ref('cssa.pay_salary_wizard').id,
            'view_mode': 'form',
            'context': {'default_payslip_id': self.id}
        }

    def newAnticipo(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Anticipo',
            'res_model': 'anticipo.wizard',
            'target': 'new',
            'view_id': self.env.ref('cssa.anticipo_wizard').id,
            'view_mode': 'form',
            'context': {'default_payslip_id': self.id}
        }

    def calculate_journey(self):
        self.km_lines = False
        self.total_km = 0
        km_dict = []

        home_query = self.employee_id.user_id.partner_id.city+","+self.employee_id.user_id.partner_id.street+","+self.employee_id.user_id.partner_id.zip
        encoded_home_query = urllib.parse.quote(home_query)

        events = self.env['calendar.event'].search(['&','&',('start_datetime','>=',self.date_from),('start_datetime','<=',self.date_to),('partner_ids','=',self.employee_id.user_id.partner_id.id)])

        i = 0
        previous_event = False
        encoded_previous_destination = False
        encoded_origin_query = encoded_home_query
        current_day = False
        encoded_destination_query = False

        for event in events.sorted(key=lambda r: r.start_datetime):
            dictionary = {
                'date': event.start_datetime,
                'name': event.name,
                'origin': '',
                'destination': '',
                'km': 0.0
            }

            #print("---------",event.name, event.start_datetime,"---------")
            opportunity = self.env['crm.lead'].search([('id','=',event.res_id)])
            task = self.env['project.task'].search([('id','=',event.res_id)])

            if opportunity.partner_id.city and opportunity.partner_id.street and opportunity.partner_id.zip:
                destination_query = opportunity.partner_id.city+","+opportunity.partner_id.street+","+opportunity.partner_id.zip
            elif task.partner_id.city and task.partner_id.street and task.partner_id.zip:
                destination_query = task.partner_id.city+","+task.partner_id.street+","+task.partner_id.zip
            else:
                continue

            encoded_destination_query = urllib.parse.quote(destination_query)

            if i == 0:
                current_day = event.start_datetime.day
            else:
                if event.start_datetime.day == current_day:
                    encoded_origin_query = encoded_previous_destination
                else:
                    encoded_home_query = self.set_destination(event)

                    dictionary = self.calculate_travel_km(encoded_previous_destination, encoded_home_query, dictionary)
                    dictionary['date'] = previous_event
                    dictionary['name'] = 'Rientro'
                    km_dict.append(dictionary)

                    dictionary = {
                        'date': event.start_datetime,
                        'name': event.name,
                        'origin': '',
                        'destination': '',
                        'km': 0.0
                    }

                    encoded_origin_query = encoded_home_query
                    current_day = event.start_datetime.day

            i += 1

            encoded_previous_destination = encoded_destination_query
            previous_event = event.start_datetime

            dictionary = self.calculate_travel_km(encoded_origin_query, encoded_destination_query, dictionary)
            km_dict.append(dictionary)

        home_query = self.employee_id.user_id.partner_id.city+","+self.employee_id.user_id.partner_id.street+","+self.employee_id.user_id.partner_id.zip
        encoded_home_query = urllib.parse.quote(home_query)
        # Return journey
        temp_dic = {
            'name': event.name,
            'origin': '',
            'destination': '',
            'km': 0.0
        }

        if encoded_destination_query:
            temp_dic = self.calculate_travel_km(encoded_destination_query, encoded_home_query, temp_dic)
            temp_dic['date'] = previous_event
            km_dict.append(temp_dic)

            lines = []
            for dic in km_dict:
                vals = {
                    'payslip_id': self._origin.id,
                    'date': dic['date'],
                    'name': dic['name'],
                    'origin': urllib.parse.unquote(dic['origin']),
                    'destination': urllib.parse.unquote(dic['destination']),
                    'km': float(dic['km'])
                }

                self.total_km += float(dic['km'])
                lines.append((0, 0, vals))

            self.km_lines = lines

    def set_destination(self, event):
        soggiorno = self.env['soggiorni'].search(['&','&',('persona','=',self.employee_id.user_id.id),('check_in','<=',event.start_datetime.date()),('check_out','>=',event.start_datetime.date())], limit=1)
        if soggiorno:
            query = soggiorno.struttura.city+","+soggiorno.struttura.street+","+soggiorno.struttura.zip
            encoded_query = urllib.parse.quote(query)
        else:
            query = self.employee_id.user_id.partner_id.city+","+self.employee_id.user_id.partner_id.street+","+self.employee_id.user_id.partner_id.zip
            encoded_query = urllib.parse.quote(query)

        return encoded_query

    def calculate_travel_km(self, origin, destination, dic):
        km = 0
        googleAPIKey = "AIzaSyA0O79j0SC2rCzSGV_5KcsuUQesEMJ7Q0Y"
        #print("Find km",origin,destination)
        dic['origin'] = origin
        dic['destination'] = destination

        google_distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin+"&destinations="+destination+"&key="+googleAPIKey
        google_distance_result = requests.get(google_distance_url)
        distance_km_json = google_distance_result.json()
        distance_km_rows = distance_km_json['rows']
        #print(distance_km_rows)
        for row in distance_km_rows:
            for element in row['elements']:
                try:
                    distance_km_txt = element['distance']['text']
                    distance_km_txt = distance_km_txt.replace('km','')
                    distance_km_txt = distance_km_txt.replace('m','')
                    km = distance_km_txt
                except:
                    print('Error: ', element, origin, destination)
                #km = re.sub("[^0-9]", "", distance_km_txt)
                #print("Km",km)
                break
            break

        dic['km'] = km
        return dic




class ProjectAnticipo(models.Model):
    _name = 'project.anticipo'

    user_id = fields.Many2one('res.users', string='Utente', index=True, tracking=True)
    partner_id = fields.Many2one('res.partner', string='Partner', index=True, tracking=True)
    anticipo = fields.Float('Anticipo')
    date_anticipo = fields.Date()

class AnticipoPaySalary(models.TransientModel):
    _name = 'anticipo.wizard'

    user_id = fields.Many2one('res.users', string='Utente', index=True, tracking=True, default=lambda self: self.env.user)
    partner_id = fields.Many2one('res.partner', string='Partner', index=True, tracking=True)
    anticipo_amount = fields.Float("Anticipo")
    payslip_id = fields.Integer()

    def action_add_anticipo(self):
        current_payslip = self.env['hr.payslip'].search([('id','=',self.payslip_id)])
        project_anticipo = self.env['project.anticipo'].search(['&',('user_id','=',self.user_id.id),('partner_id','=',self.partner_id.id)])

        if project_anticipo:
            project_anticipo.anticipo += self.anticipo_amount
        else:
            vals = {
                'user_id': current_payslip.employee_id.user_id.id,
                'partner_id': self.partner_id.id,
                'anticipo': self.anticipo_amount,
                'date_anticipo': current_payslip.date_to
            }
            print("vals", vals)

            self.env['project.anticipo'].create(vals)

    def reset_anticipo(self):
        current_payslip = self.env['hr.payslip'].search([('id','=',self.payslip_id)])
        project_anticipo = self.env['project.anticipo'].search(['&',('user_id','=',current_payslip.employee_id.user_id.id),('partner_id','=',self.partner_id.id)])

        for anticipo in project_anticipo:
            anticipo.unlink()


class PaySalaryWizard(models.TransientModel):
    _name = 'pay.salary.wizard'

    payslip_id = fields.Integer()
    pay_salary_amount = fields.Float('Importo da pagare')
    tax_amount = fields.Float('Contributi')

    def action_pay_salary(self):
        current_payslip = self.env['hr.payslip'].search([('id','=',self.payslip_id)])
        if current_payslip.total_payable > 0:
            current_payslip.total_payable -= self.pay_salary_amount

        if self.tax_amount > 0:
            self.create_account_move_line(current_payslip)
        else:
            self.create_purchase_order(current_payslip)

        current_payslip.employee_id.user_id.go_count_container += (current_payslip.employee_id.user_id.go_count // 20)*20
        current_payslip.employee_id.user_id.check_count_container += (current_payslip.employee_id.user_id.check_count // 40)*40


    def create_purchase_order(self, current_payslip):
        partner_id = self.env['res.partner'].search([('id','=',current_payslip.employee_id.user_id.partner_id.id)])

        vals = {
            'date_approve': current_payslip.date_from,
            'company_id': self.env.user.company_id.id,
            'currency_id': self.env.user.company_id.currency_id.id,
            'date_order': date.today(),
            'partner_id': partner_id.id,
            #'state': 'purchase'
        }
        purchase_order = self.env['purchase.order'].create(vals)

        order_lines = []
        for product in current_payslip.products:
            product_found = False
            for l in order_lines:
                if l['product_id'] == product['product_id'] and l['price_unit'] == product['amount'] and l['name'] == partner_id.name:
                    l['product_qty'] += 1
                    product_found = True

            if not product_found:
                line = {
                    'name': product['name'],
                    'order_id': purchase_order.id,
                    'product_id': product['product_id'],
                    'product_qty': 1.0,
                    'price_unit': product['amount'],
                    'product_uom': product['uom'],
                    'date_planned': current_payslip.date_from
                }
                order_lines.append(line)

        self.env['purchase.order.line'].create(order_lines)

    def create_account_move_line(self, current_payslip):
        journal_id = self.env['account.journal'].search([('name','=','Personale')]).id
        partner_id = self.env['res.partner'].search([('id','=',current_payslip.employee_id.user_id.partner_id.id)]).id
        vals = {
            'date': date.today(),
            'journal_id': journal_id,
            'partner_id': partner_id
        }
        new_move = self.env['account.move'].create(vals)

        stipendio_conto_economico = self.env['account.account'].search([('code','=','75.00.001')]).id
        contributi_conto_economico = self.env['account.account'].search([('code','=','75.00.002')]).id
        stipendio_conto_patrimoniale = self.env['account.account'].search([('code','=','49.27.041')]).id
        contributi_conto_patrimoniale = self.env['account.account'].search([('code','=','49.25.505')]).id

        account_total = self.pay_salary_amount+self.tax_amount
        new_move.amount_total_signed = account_total

        line_1 = {
            'move_id': new_move.id,
            'account_id': stipendio_conto_economico,
            'credit': 0,
            'debit': self.pay_salary_amount
        }
        line_2 = {
            'move_id': new_move.id,
            'account_id': contributi_conto_economico,
            'credit': 0,
            'debit': self.tax_amount
        }
        line_3 = {
            'move_id': new_move.id,
            'account_id': stipendio_conto_patrimoniale,
            'credit': self.pay_salary_amount,
            'debit': 0
        }
        line_4 = {
            'move_id': new_move.id,
            'account_id': contributi_conto_patrimoniale,
            'credit': self.tax_amount,
            'debit': 0
        }

        self.env['account.move.line'].create([line_1, line_2, line_3, line_4])