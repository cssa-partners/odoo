# -*- coding: utf-8 -*-

from odoo import models, fields, api

class MailActivity(models.Model):
    _inherit = 'mail.activity'

    orario = fields.Char('Orario')

    user_id = fields.Many2one(
        'res.users', 'Assigned to',
        default=lambda self: self.env.user,
        index=True, required=True)

    @api.onchange('orario')
    def _onchange_orario(self):
        opportunity = self.env['crm.lead'].search([('id','=',self.res_id)])
        opportunity.orario = self.orario

    @api.onchange('user_id')
    def _onchange_user_id(self):
        current_user = self.env.user
        is_cliente = False
        for group in current_user.groups_id:
            if group.name == 'Cliente':
                is_cliente = True
                break

        if is_cliente:
            users = self.env['res.users'].search([('groups_id.name','=','Cliente')])
            return {'domain': {'user_id': [('id', 'in', users.ids)]}}
