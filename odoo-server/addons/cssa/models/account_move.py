# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.misc import formatLang
from odoo.tools import float_compare
import re
import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools


class AccountMove(models.Model):
    _inherit = 'account.move'

    legale = fields.Boolean('Legale')
    anticipo = fields.Boolean('Anticipo')
    auto_line_update = fields.Boolean('Autocompletamento')
    is_wrong = fields.Boolean('Errata')
    ritenuta_updated = fields.Boolean()

    importo_ritenuta = fields.Float('Importo ritenuta')
    aliquota_ritenuta = fields.Float('Aliquota ritenuta')

    importo_cassa = fields.Float('Imponibile cassa previdenziale')
    aliquota_cassa = fields.Float('Aliquota cassa previdenziale')

    payment_method = fields.Selection([
        ('Bonifico bancario', 'Bonifico bancario'),
        ('Assegno','Assegno'),
        ('Riba', 'Riba')
    ],'Metodo di pagamento')

    different_installments = fields.Boolean("Termini di pagamento diversi dall' ordine")

    @api.model
    def create(self, vals):
        account_move = super(AccountMove, self).create(vals)
        sale_order = self.env['sale.order'].search([('invoice_ids','=',account_move.id)])
        if sale_order.esito == 'Da effettuare':
            sale_order.esito = False

        return account_move


    def write(self, vals):
        invoice = super(AccountMove, self).write(vals)
        if self.legale:
            for line in self.line_ids:
                if line.date_maturity:
                    line.name += " (legale)"
        else:
            for line in self.line_ids:
                if line.name and "(legale)" in line.name:
                    line.name = line.name.replace(" (legale)","")

        if self.auto_line_update:
            first_prod = self.line_ids[0].product_id
            for line in self.line_ids:
                if line.product_id.id != first_prod.id:
                    line.product_id = first_prod.id
            for in_line in self.invoice_line_ids:
                in_line.account_id = first_prod.property_account_expense_id.id

        if self.anticipo:
            for line in self.line_ids:
                if line.date_maturity:
                    line.tipologia = 'Anticipo'
        else:
            sale_order = self.env['sale.order'].search([('invoice_ids','=',self._origin.id)])
            if sale_order.type == 'Check' and sale_order.esito == False:
                for line in self.line_ids:
                    if line.date_maturity:
                        line.tipologia = 'Check'
            else:
                counter = 0
                for line in self.line_ids:
                    if line.date_maturity:
                        counter += 1

                i = 1
                for line in self.line_ids:
                    if line.date_maturity:
                        if i == counter:
                            line.tipologia = 'Saldo'
                        else:
                            line.tipologia = str(i)+"/"+str(counter)
                        i += 1

        return invoice

    def setNoProvvigione(self):
        self.ref = 'no_provvigione'

    def update_iva(self):
        for line in self.line_ids:
            if line.account_id.code == '28.11.018':
                new_account = self.env['account.account'].search([('code','=','28.11.017')])
                line.account_id = new_account.id
                break
            elif line.account_id.code == '49.23.012':
                new_account = self.env['account.account'].search([('code','=','49.23.013')])
                line.account_id = new_account.id
                break

    @api.depends(
        'line_ids.debit',
        'line_ids.credit',
        'line_ids.currency_id',
        'line_ids.amount_currency',
        'line_ids.amount_residual',
        'line_ids.amount_residual_currency',
        'line_ids.payment_id.state')
    def _compute_amount(self):
        invoice_ids = [move.id for move in self if move.id and move.is_invoice(include_receipts=True)]
        self.env['account.payment'].flush(['state'])
        if invoice_ids:
            self._cr.execute(
                '''
                    SELECT move.id
                    FROM account_move move
                    JOIN account_move_line line ON line.move_id = move.id
                    JOIN account_partial_reconcile part ON part.debit_move_id = line.id OR part.credit_move_id = line.id
                    JOIN account_move_line rec_line ON
                        (rec_line.id = part.debit_move_id AND line.id = part.credit_move_id)
                    JOIN account_payment payment ON payment.id = rec_line.payment_id
                    JOIN account_journal journal ON journal.id = rec_line.journal_id
                    WHERE payment.state IN ('posted', 'sent')
                    AND journal.post_at = 'bank_rec'
                    AND move.id IN %s
                UNION
                    SELECT move.id
                    FROM account_move move
                    JOIN account_move_line line ON line.move_id = move.id
                    JOIN account_partial_reconcile part ON part.debit_move_id = line.id OR part.credit_move_id = line.id
                    JOIN account_move_line rec_line ON
                        (rec_line.id = part.credit_move_id AND line.id = part.debit_move_id)
                    JOIN account_payment payment ON payment.id = rec_line.payment_id
                    JOIN account_journal journal ON journal.id = rec_line.journal_id
                    WHERE payment.state IN ('posted', 'sent')
                    AND journal.post_at = 'bank_rec'
                    AND move.id IN %s
                ''', [tuple(invoice_ids), tuple(invoice_ids)]
            )
            in_payment_set = set(res[0] for res in self._cr.fetchall())
        else:
            in_payment_set = {}

        for move in self:
            total_untaxed = 0.0
            total_untaxed_currency = 0.0
            total_tax = 0.0
            total_tax_currency = 0.0
            total_residual = 0.0
            total_residual_currency = 0.0
            total = 0.0
            total_currency = 0.0
            currencies = set()

            for line in move.line_ids:
                if line.currency_id:
                    currencies.add(line.currency_id)

                if move.is_invoice(include_receipts=True):
                    # === Invoices ===

                    if not line.exclude_from_invoice_tab:
                        # Untaxed amount.
                        total_untaxed += line.balance
                        total_untaxed_currency += line.amount_currency
                        total += line.balance
                        total_currency += line.amount_currency
                    elif line.tax_line_id:
                        # Tax amount.
                        total_tax += line.balance
                        total_tax_currency += line.amount_currency
                        total += line.balance
                        total_currency += line.amount_currency
                    elif line.account_id.user_type_id.type in ('receivable', 'payable'):
                        # Residual amount.
                        total_residual += line.amount_residual
                        total_residual_currency += line.amount_residual_currency
                else:
                    # === Miscellaneous journal entry ===
                    if line.debit:
                        total += line.balance
                        total_currency += line.amount_currency

            if move.type == 'entry' or move.is_outbound():
                sign = 1
            else:
                sign = -1
            move.amount_untaxed = sign * (total_untaxed_currency if len(currencies) == 1 else total_untaxed)
            move.amount_tax = sign * (total_tax_currency if len(currencies) == 1 else total_tax)
            move.amount_total = sign * (total_currency if len(currencies) == 1 else total)
            move.amount_residual = -sign * (total_residual_currency if len(currencies) == 1 else total_residual)
            move.amount_untaxed_signed = -total_untaxed
            move.amount_tax_signed = -total_tax
            move.amount_total_signed = abs(total) if move.type == 'entry' else -total
            move.amount_residual_signed = total_residual

            currency = len(currencies) == 1 and currencies.pop() or move.company_id.currency_id
            is_paid = currency and currency.is_zero(move.amount_residual) or not move.amount_residual

            # Compute 'invoice_payment_state'.
            if move.type == 'entry':
                move.invoice_payment_state = False
            elif move.state == 'posted' and is_paid:
                if move.id in in_payment_set:
                    move.invoice_payment_state = 'in_payment'
                else:
                    move.invoice_payment_state = 'paid'
                    self.update_iva()
            else:
                move.invoice_payment_state = 'not_paid'


    @api.depends('line_ids.price_subtotal', 'line_ids.tax_base_amount', 'line_ids.tax_line_id', 'partner_id', 'currency_id')
    def _compute_invoice_taxes_by_group(self):
        ''' Helper to get the taxes grouped according their account.tax.group.
        This method is only used when printing the invoice.
        '''

        for move in self:
            lang_env = move.with_context(lang=move.partner_id.lang).env
            tax_lines = move.line_ids.filtered(lambda line: line.tax_line_id)
            tax_balance_multiplicator = -1 if move.is_inbound(True) else 1
            res = {}
            # There are as many tax line as there are repartition lines
            done_taxes = set()
            for line in tax_lines:
                res.setdefault(line.tax_line_id.tax_group_id, {'base': 0.0, 'amount': 0.0})
                res[line.tax_line_id.tax_group_id]['amount'] += tax_balance_multiplicator * (line.amount_currency if line.currency_id else line.balance)
                tax_key_add_base = tuple(move._get_tax_key_for_group_add_base(line))
                if tax_key_add_base not in done_taxes:
                    if line.currency_id and line.company_currency_id and line.currency_id != line.company_currency_id:
                        amount = line.company_currency_id._convert(line.tax_base_amount, line.currency_id, line.company_id, line.date or fields.Date.today())
                    else:
                        amount = line.tax_base_amount
                    res[line.tax_line_id.tax_group_id]['base'] += amount
                    # The base should be added ONCE
                    done_taxes.add(tax_key_add_base)

            # At this point we only want to keep the taxes with a zero amount since they do not
            # generate a tax line.
            for line in move.line_ids:
                for tax in line.tax_ids.flatten_taxes_hierarchy().filtered(lambda t: t.amount == 0.0):
                    res.setdefault(tax.tax_group_id, {'base': 0.0, 'amount': 0.0})
                    res[tax.tax_group_id]['base'] += tax_balance_multiplicator * (line.amount_currency if line.currency_id else line.balance)

            res = sorted(res.items(), key=lambda l: l[0].sequence)

            move.amount_by_group = [(
                group.name, amounts['amount'],
                amounts['base'],
                formatLang(lang_env, amounts['amount'], currency_obj=move.currency_id),
                formatLang(lang_env, amounts['base'], currency_obj=move.currency_id),
                len(res),
                group.id
            ) for group, amounts in res]

            if move.importo_ritenuta > 0 and not move.ritenuta_updated:
                move.amount_residual -= move.importo_ritenuta
                self.ritenuta_updated = True
                move.ritenuta_updated = True

            # move.amount_untaxed = 0
            # for line in move.invoice_line_ids:
            #     if not line.tax_ids.l10n_it_has_exoneration:
            #         move.amount_untaxed += line.price_subtotal


    @api.onchange('payment_method')
    def _onchange_payment_method(self):
        if self.payment_method:
            bank_name = self.company_id.bank_ids[0].bank_id.name
            bank_iban = "IBAN: " + self.company_id.bank_ids[0].bank_id.iban
            bank_bic = "BIC " + self.company_id.bank_ids[0].bank_id.bic
            bank_conto = "Conto: " + self.company_id.bank_ids[0].journal_id[0].bank_acc_number

            if len(self.invoice_line_ids) > 1:
                for line in self.invoice_line_ids:
                    if 'Anticipo' in line.product_id.name:
                        line.price_unit = -line.price_unit
                        line.quantity = 1
                        break

            sale_order = self.env['sale.order'].search([('invoice_ids','=',self._origin.id)])
            self.narration = "Pagamento tramite " + self.payment_method + "\n\n"

            if len(sale_order.installment_lines) > 1 and not self._origin.anticipo:
                i = 0
                for installment in sale_order.installment_lines:
                    i += 1
                    if i == 1:
                        continue
                    self.narration += installment.value_amount + " con scadenza il " + (str(installment.due_date.day)+"/"+str(installment.due_date.month)+"/"+str(installment.due_date.year)) + "\n"

                self.narration += "\n\n"
            self.narration += bank_name + "\n" + bank_iban + "\n" + bank_bic + "\n" + bank_conto

    def migrate_amount_total_signed(self):
        moves = self.env['account.move'].search([('journal_id','=',20)])
        for move in moves:
            move.amount_total_signed = -move.amount_total_signed

    def migrate_line(self):
        thread = threading.Thread(target=self.migrate_l, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_l(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)

                all_invoices = new_env['account.move'].search([('type','=','in_invoice')])
                for invoice in all_invoices:
                    try:
                        invoice.button_draft()
                        print("Changed")
                    except:
                        print("Bac")
                    # for line in invoice.line_ids:
                    #     if line.product_id:
                    #         line.debit = line.price_unit+(line.price_unit*line.tax_ids[0].amount)/100
                    #         line.with_context(check_move_validity=False).exclude_from_invoice_tab = False
                    #     else:
                    #         line.with_context(check_move_validity=False).exclude_from_invoice_tab = True
                    #     print("Done")

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    tipologia = fields.Char('Tipologia')
    note = fields.Char('Note')
    note_exist = fields.Char('Esistono note')

    def _get_computed_price_unit(self):
        self.ensure_one()

        if not self.product_id or self.product_id.no_update:
            return self.price_unit
        elif self.move_id.is_sale_document(include_receipts=True):
            # Out invoice.
            price_unit = self.product_id.lst_price
        elif self.move_id.is_purchase_document(include_receipts=True):
            # In invoice.
            price_unit = self.product_id.standard_price
        else:
            return self.price_unit

        if self.product_uom_id != self.product_id.uom_id:
            price_unit = self.product_id.uom_id._compute_price(price_unit, self.product_uom_id)

        return price_unit

    def _get_computed_taxes(self):
        self.ensure_one()

        if self.move_id.is_sale_document(include_receipts=True):
            # Out invoice.
            if self.product_id.taxes_id:
                tax_ids = self.product_id.taxes_id.filtered(lambda tax: tax.company_id == self.move_id.company_id)
            elif self.account_id.tax_ids:
                tax_ids = self.account_id.tax_ids
            else:
                tax_ids = self.env['account.tax']
            if not tax_ids and not self.exclude_from_invoice_tab:
                tax_ids = self.move_id.company_id.account_sale_tax_id
        elif self.move_id.is_purchase_document(include_receipts=True):
            # In invoice.
            if self.product_id.no_update:
                tax_ids = self.tax_ids
            elif self.product_id.supplier_taxes_id:
                tax_ids = self.product_id.supplier_taxes_id.filtered(lambda tax: tax.company_id == self.move_id.company_id)
            elif self.account_id.tax_ids:
                tax_ids = self.account_id.tax_ids
            else:
                tax_ids = self.env['account.tax']
            if not tax_ids and not self.exclude_from_invoice_tab:
                tax_ids = self.move_id.company_id.account_purchase_tax_id
        else:
            # Miscellaneous operation.
            tax_ids = self.account_id.tax_ids

        if self.company_id and tax_ids:
            tax_ids = tax_ids.filtered(lambda tax: tax.company_id == self.company_id)

        return tax_ids

    @api.onchange('product_id')
    def _onchange_product_id(self):
        for line in self:
            if not line.product_id or line.display_type in ('line_section', 'line_note'):
                continue

            if not line.product_id.no_update:
                line.name = line._get_computed_name()

            line.account_id = line._get_computed_account()
            line.tax_ids = line._get_computed_taxes()
            line.product_uom_id = line._get_computed_uom()
            line.price_unit = line._get_computed_price_unit()

            # Manage the fiscal position after that and adapt the price_unit.
            # E.g. mapping a price-included-tax to a price-excluded-tax must
            # remove the tax amount from the price_unit.
            # However, mapping a price-included tax to another price-included tax must preserve the balance but
            # adapt the price_unit to the new tax.
            # E.g. mapping a 10% price-included tax to a 20% price-included tax for a price_unit of 110 should preserve
            # 100 as balance but set 120 as price_unit.
            if line.tax_ids and line.move_id.fiscal_position_id:
                line.price_unit = line._get_price_total_and_subtotal()['price_subtotal']
                line.tax_ids = line.move_id.fiscal_position_id.map_tax(line.tax_ids._origin, partner=line.move_id.partner_id)
                accounting_vals = line._get_fields_onchange_subtotal(price_subtotal=line.price_unit, currency=line.move_id.company_currency_id)
                balance = accounting_vals['debit'] - accounting_vals['credit']
                line.price_unit = line._get_fields_onchange_balance(balance=balance).get('price_unit', line.price_unit)

            # Convert the unit price to the invoice's currency.
            company = line.move_id.company_id
            line.price_unit = company.currency_id._convert(line.price_unit, line.move_id.currency_id, company, line.move_id.date)

        if len(self) == 1:
            return {'domain': {'product_uom_id': [('category_id', '=', self.product_uom_id.category_id.id)]}}
