# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Company(models.Model):
    _inherit = 'res.company'

    società_check_doc_1 = fields.Char("Documento 1")
    società_check_doc_2 = fields.Char("Documento 2")
    società_check_doc_3 = fields.Char("Documento 3")
    società_check_doc_4 = fields.Char("Documento 4")

    ditta_check_doc_1 = fields.Char("Documento 1")
    ditta_check_doc_2 = fields.Char("Documento 2")
    ditta_check_doc_3 = fields.Char("Documento 3")
    ditta_check_doc_4 = fields.Char("Documento 4")

    n_spazio_analisi = fields.Integer(default=0)

    def reset_n_spazio_analisi(self):
        current_company = self.env['res.company'].search([('id','=',self.env.user.company_id.id)])
        current_company.n_spazio_analisi = 0