# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from datetime import date


class Bank(models.Model):
    _inherit = 'res.bank'

    iban = fields.Char('IBAN')