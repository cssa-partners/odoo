# -*- coding: utf-8 -*-
from odoo import http


class Doceasy(http.Controller):
    @http.route('/doceasy/download_invoices', type='json', auth="public")
    def download_invoices(self):
        http.request.env['doceasy'].loadPassiveInvoices()
        return True


