# -*- coding: utf-8 -*-

from odoo import models, api, fields
from datetime import date

import base64
import json
import requests
import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools
import re
import sys
import chilkat
import os

glob = chilkat.CkGlobal()
success = glob.UnlockBundle("CSSAPA.CB1062022_Bh6zMLSm4Rlu")
if (success != True):
    print(glob.lastErrorText())
    sys.exit()

status = glob.get_UnlockStatus()
if (status == 2):
    print("Unlocked using purchased unlock code.")
else:
    print("Unlocked in trial mode.")


class Doceasy(models.Model):
    _name = 'doceasy'

    base_url = "https://webapi.doceasy.it"
    key = "a8123c1c13c6ec632acd434f863d13f184d9a28723a5e2b4e6d7d2d6ad3b568d"
    secret = "f657097ffbc10a103f0021331f36db9a7f90877346522a2b7e95df22e3088e250500c550e5ce42509aeee46b85248c09c5509ea572749aad9b2926b697d347be"
    codice_cliente = "09985870964"
    last_id = ""

    def loadPassiveInvoices(self):
        thread = threading.Thread(target=self.load, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def load(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)

                invoices = new_env['account.move'].search(['&',('journal_id','=',2),('type','=','in_invoice')]).sorted(key=lambda r: r.id)
                if len(invoices) > 0:
                    last_id = invoices[len(invoices)-1].sdi_id

                print("Last ", last_id)

                endpoint = "/api/DocumentoPassivo/elenco/"+str(last_id)
                #endpoint = "/api/DocumentoPassivo/elenco/"
                url = self.base_url+endpoint

                headers = {
                    'APIKey': self.key,
                    'APISecret': self.secret,
                    'CodiceCliente': self.codice_cliente
                }
                invoices = requests.get(url, headers=headers)
                invoices_json = invoices.json()
                count = 0
                for invoice in invoices_json:
                    if '2021' not in invoice['Data']:
                        continue

                    # print(invoice['ID'])
                    # print(invoice['Denominazione'])
                    # print(invoice['Numero'])
                    # print(invoice['Data'])
                    # print(invoice['Importo'])
                    # print(invoice['PartitaIva'])
                    # print(invoice['CodiceFiscale'])
                    # print(invoice['DataRicezione'])

                    partner = self.findOrCreatePartner(invoice, new_env)
                    #self.downloadInvoice(invoice, partner, new_env)
                    try:
                        self.downloadInvoice(invoice, partner, new_env)
                    except:
                        print(invoice['Denominazione'])
                        print(invoice['Numero'])

                    # count += 1
                    # if count == 80:
                    #     break

                    print('###############################################################')

                #self.cleanInvoices(new_env)


    def findOrCreatePartner(self, invoice, new_env):
        partner = new_env['res.partner'].search([('name','=',invoice['Denominazione'])], limit=1)

        if not partner:
            property_account_payable_id = new_env['account.account'].search([('name','=','FORNITORE')]).id
            property_account_receivable_id = new_env['account.account'].search([('name','=','CREDITI V/CLIENTI')]).id
            new_partner = {
                'name': invoice['Denominazione'],
                'property_account_payable_id': property_account_payable_id,
                'property_account_receivable_id': property_account_receivable_id,
                'l10n_it_codice_fiscale': invoice['CodiceFiscale'] if invoice['CodiceFiscale'] else invoice['PartitaIva'],
                'partita_iva': invoice['PartitaIva'],
                'vat': invoice['PartitaIva'],
                'company_type': 'company',
                'supplier_rank': 1,
                'relationship': 'Fornitore'
            }

            partner = new_env['res.partner'].create(new_partner)

        return partner

    def downloadInvoice(self, invoice, partner, new_env):
        endpoint = "/api/DocumentoPassivo/"+str(invoice['ID'])+"/file"
        url = self.base_url+endpoint

        headers = {
            'APIKey': self.key,
            'APISecret': self.secret,
            'CodiceCliente': self.codice_cliente,
            'Accept': 'application/json'
        }
        invoice_response = requests.get(url, headers=headers)
        self.parseInvoice(invoice_response, invoice, partner, new_env)

    def decryptInvoice(self, invoice_content):
        f = open("certificates/encrypt.p7m", "wb")
        f.write(invoice_content.content)
        f.close()

        crypt = chilkat.CkCrypt2()

        outputFile = "certificates/original.xml"
        inFile = "certificates/encrypt.p7m"

        # Verify the signature and extract the XML file.
        success = crypt.VerifyP7M(inFile,outputFile)
        if (success == False):
            print(crypt.lastErrorText())
            sys.exit()

        new_file = open(outputFile, "r")

        os.remove(outputFile)
        os.remove(inFile)

        return new_file.read()


    def parseInvoice(self, invoice, invoice_res, partner, new_env):
        if 'p7m' in invoice_res['NomeFile']:
            invoice_content = self.decryptInvoice(invoice)
        else:
            invoice_content = invoice.content.decode('utf-8')

        codice_destinatario_start = invoice_content.find('<CodiceDestinatario>')
        codice_destinatario_end = invoice_content.find('</CodiceDestinatario>')
        codice_destinatario = invoice_content[codice_destinatario_start+len('<CodiceDestinatario>'):codice_destinatario_end]

        indirizzo_start = invoice_content.find('<Indirizzo>')
        indirizzo_end = invoice_content.find('</Indirizzo>')
        indirizzo = invoice_content[indirizzo_start+len('<Indirizzo>'):indirizzo_end]

        cap_start = invoice_content.find('<CAP>')
        cap_end = invoice_content.find('</CAP>')
        cap = invoice_content[cap_start+len('<CAP>'):cap_end]

        city_start = invoice_content.find('<Comune>')
        city_end = invoice_content.find('</Comune>')
        city = invoice_content[city_start+len('<Comune>'):city_end]

        # Update Partner
        partner.l10n_it_pa_index = codice_destinatario
        partner.street = indirizzo
        partner.zip = cap
        partner.city = city
        partner.company_type = 'company'
        partner.country_id = 109
        partner.relationship = 'Fornitore'

        if '<Provincia>' in invoice_content:
            provincia_start = invoice_content.find('<Provincia>')
            provincia_end = invoice_content.find('</Provincia>')
            provincia = invoice_content[provincia_start+len('<Provincia>'):provincia_end]
            partner.provincia = provincia
        else:
            partner.provincia = ""

        # Create invoice
        type_invoice = 'in_invoice' if invoice_res['TipoDocumento'] != 'TD04' else 'in_refund'
        new_invoice = {
            'currency_id': 1,
            'invoice_date': invoice_res['Data'],
            'date': invoice_res['DataRicezione'],
            'journal_id': 2,
            'type': type_invoice,
            'ref': str(invoice_res['ID']) + ". Numero: " + str(invoice_res['Numero']),
            'partner_id': partner.id,
            'sdi_id': invoice_res['ID']
        }
        print("AVAX", new_invoice)
        db_invoice =  new_env['account.move'].create(new_invoice)

        # if '<DataScadenzaPagamento>' in invoice_content:
        #     data_scadenza_start = invoice_content.find('<DataScadenzaPagamento>')
        #     data_scadenza_end = invoice_content.find('</DataScadenzaPagamento>')
        #     data_scadenza = invoice_content[data_scadenza_start+len('<DataScadenzaPagamento>'):data_scadenza_end]
        #
        #     db_invoice.invoice_date_due = data_scadenza

        invoice_body_start = invoice_content.find('<DatiBeniServizi>')
        invoice_body_end = invoice_content.find('</DatiBeniServizi>')
        invoice_body = invoice_content[invoice_body_start+len('<DatiBeniServizi>'):invoice_body_end]

        dettaglio_linee_start = [i for i in range(len(invoice_body)) if invoice_body.startswith('<DettaglioLinee>', i)]
        dettaglio_linee_end = [i for i in range(len(invoice_body)) if invoice_body.startswith('</DettaglioLinee>', i)]

        invoice_lines = []

        j = 0
        for line in dettaglio_linee_start:
            if j < len(dettaglio_linee_end):
                line_body = invoice_body[line+len('<DettaglioLinee>'):dettaglio_linee_end[j]]

                descrizione_start = line_body.find('<Descrizione>')
                descrizione_end = line_body.find('</Descrizione>')
                descrizione = line_body[descrizione_start+len('<Descrizione>'):descrizione_end]

                quantity_int = 1.0
                if '<Quantita>' in line_body:
                    quantity_start = line_body.find('<Quantita>')
                    quantity_end = line_body.find('</Quantita>')
                    quantity = line_body[quantity_start+len('<Quantita>'):quantity_end]
                    quantity = re.findall("\d+\.\d+", quantity)

                    if len(quantity) > 0:
                        quantity_int = float(quantity[0])

                prezzo_start = line_body.find('<PrezzoUnitario>')
                prezzo_end = line_body.find('</PrezzoUnitario>')
                prezzo = line_body[prezzo_start+len('<PrezzoUnitario>'):prezzo_end]

                try:
                    prezzo = float(prezzo)
                except:
                    prezzo = 0

                aliquota_start = line_body.find('IVA>')
                aliquota_end = line_body.find('</AliquotaIVA>')
                aliquota = line_body[aliquota_start+len('IVA>'):aliquota_end]
                aliquota = re.findall("\d+\.\d+", aliquota)
                if len(aliquota) > 0:
                    aliquota = float(aliquota[0])
                else:
                    aliquota = 0

                print("Descrizione: ", descrizione)
                print("PrezzoUnitario: ", prezzo)
                print("AliquotaIVA: ", aliquota)

                #Conto Generico
                conto_generico = new_env['account.account'].search([('code','=','0')]).id

                line = {
                    'move_id': db_invoice.id,
                    'name': descrizione,
                    'account_id': conto_generico,
                    'price_unit': float(prezzo),
                    'quantity': quantity_int,
                    'exclude_from_invoice_tab': False
                }
                print(line)

                #Find Tax
                aliquota_int = int(round(float(aliquota),0))

                if aliquota_int > 0:
                    imposta = "Iva al " + str(aliquota_int) + "% (credito)"
                    imposta_model = new_env['account.tax'].search([('name','=',imposta)], limit=1)

                    imposta_id = imposta_model.id
                    print("Imposta", imposta_id)
                    if imposta_id:
                        line['tax_ids'] = [imposta_model.id]

                invoice_lines.append(line)
                j+=1


        if '<DatiCassaPrevidenziale>' in invoice_content:
            cassa_previdenziale_start = invoice_content.find('<DatiCassaPrevidenziale>')
            cassa_previdenziale_end = invoice_content.find('</DatiCassaPrevidenziale>')
            cassa_previdenziale = invoice_content[cassa_previdenziale_start+len('<DatiCassaPrevidenziale>'):cassa_previdenziale_end]

            cassa_previdenziale_aliquota_start = cassa_previdenziale.find('<AlCassa>')
            cassa_previdenziale_aliquota_end = cassa_previdenziale.find('</AlCassa>')
            cassa_previdenziale_aliquota = cassa_previdenziale[cassa_previdenziale_aliquota_start+len('<AlCassa>'):cassa_previdenziale_aliquota_end]
            print("cassa_previdenziale_aliquota",cassa_previdenziale_aliquota)
            cassa_previdenziale_imponibile_start = cassa_previdenziale.find('<ImponibileCassa>')
            cassa_previdenziale_imponibile_end = cassa_previdenziale.find('</ImponibileCassa>')
            cassa_previdenziale_imponibile = cassa_previdenziale[cassa_previdenziale_imponibile_start+len('<ImponibileCassa>'):cassa_previdenziale_imponibile_end]

            db_invoice.importo_cassa = float(cassa_previdenziale_imponibile)
            db_invoice.aliquota_cassa = float(cassa_previdenziale_aliquota)

            cassa_previdenziale = (db_invoice.importo_cassa*db_invoice.aliquota_cassa)/100

            imposta = "Iva al 22% (credito)"
            imposta_model = new_env['account.tax'].search([('name','=',imposta)], limit=1)

            cassa_line = {
                'move_id': db_invoice.id,
                'account_id': conto_generico,
                'exclude_from_invoice_tab': False,
                'name': 'CASSA PREVIDENZIALE',
                'price_unit': cassa_previdenziale,
                'quantity': 1,
                'tax_ids': [imposta_model.id]
            }

            invoice_lines.append(cassa_line)


        if '<DatiRitenuta>' in invoice_content:
            ritenuta_start = invoice_content.find('<DatiRitenuta>')
            ritenuta_end = invoice_content.find('</DatiRitenuta>')
            ritenuta_body = invoice_content[ritenuta_start+len('<DatiRitenuta>'):ritenuta_end]

            importo_ritenuta_start = ritenuta_body.find('<ImportoRitenuta>')
            importo_ritenuta_end = ritenuta_body.find('</ImportoRitenuta>')
            importo_ritenuta_body = ritenuta_body[importo_ritenuta_start+len('<ImportoRitenuta>'):importo_ritenuta_end]

            aliquota_ritenuta_start = ritenuta_body.find('<AliquotaRitenuta>')
            aliquota_ritenuta_end = ritenuta_body.find('</AliquotaRitenuta>')
            aliquota_ritenuta_body = ritenuta_body[aliquota_ritenuta_start+len('<AliquotaRitenuta>'):aliquota_ritenuta_end]

            db_invoice.importo_ritenuta = float(importo_ritenuta_body)
            db_invoice.aliquota_ritenuta = float(aliquota_ritenuta_body)

        new_env['account.move.line'].with_context(check_move_validity=False).create(invoice_lines)

        dati_pagamento_start = invoice_content.find('<DatiPagamento>')
        dati_pagamento_end = invoice_content.find('</DatiPagamento>')
        dati_pagamento = invoice_content[dati_pagamento_start+len('<DatiPagamento>'):dati_pagamento_end]

        dettaglio_pagamento_start = [y for y in range(len(dati_pagamento)) if dati_pagamento.startswith('<DettaglioPagamento>', y)]
        dettaglio_pagamento_end = [y for y in range(len(dati_pagamento)) if dati_pagamento.startswith('</DettaglioPagamento>', y)]

        z = 0
        payment_terms = []
        for dati in dettaglio_pagamento_start:
            if z < len(dettaglio_pagamento_end):
                line_body = dati_pagamento[dati+len('<DettaglioPagamento>'):dettaglio_pagamento_end[z]]

                data_scadenza_start = line_body.find('<DataScadenzaPagamento>')
                data_scadenza_end = line_body.find('</DataScadenzaPagamento>')
                importo_pag_start = line_body.find('<ImportoPagamento>')
                importo_pag_end = line_body.find('</ImportoPagamento>')

                data_scadenza = line_body[data_scadenza_start+len('<DataScadenzaPagamento>'):data_scadenza_end]
                importo_pag = line_body[importo_pag_start+len('<ImportoPagamento>'):importo_pag_end]

                payment_terms.append({
                    "data": data_scadenza,
                    "importo": importo_pag
                })

                z += 1

        if '<DataScadenzaPagamento>' in invoice_content:
            if len(payment_terms) == 1:
                db_invoice.invoice_date_due = payment_terms[0]['data']
            elif len(payment_terms) > 1:
                days = 30
                n = 2
                term_name = "30gg"
                db_invoice.scadenze = ""
                for term in payment_terms:
                    if n > len(payment_terms):
                        break

                    db_invoice.scadenze += term['importo']+" il "+term['data'] + " - "
                    term_name += " "+str(days*n) + "gg"
                    n += 1

                pay_term = new_env['account.payment.term'].search([('name','=',term_name)])
                print("manicomio1",pay_term)
                if pay_term:
                    db_invoice.invoice_payment_term_id = pay_term.id


    def uploadInvoice(self, invoice):
        endpoint = "/api/DocumentoAttivo"
        url = self.base_url+endpoint

        headers = {
            'APIKey': self.key,
            'APISecret': self.secret,
            'CodiceCliente': self.codice_cliente,
            'Content-Type': 'application/json'
        }

        message_bytes = base64.b64decode(invoice.datas)
        invoice_data = message_bytes.decode()

        invoice_data = invoice_data.encode()
        invoice_data = base64.b64encode(invoice_data)
        invoice_data = invoice_data.decode('utf-8')

        body = {
            "NomeFile": invoice.name,
            "DatiDocumento": invoice_data
        }

        body = json.dumps(body)

        upload_res = requests.post(url, headers=headers, data=body)
        print("Response content ", upload_res.content)
        print("Response ", upload_res)

    def cleanInvoices(self, new_env):
        invoices = new_env['account.move'].search(['&',('amount_total_signed','=',0),('state','=','draft')])
        for invoice in invoices:
            invoice.unlink()


class AccountMove(models.Model):
    _inherit = 'account.move'

    sdi_id = fields.Char()
    scadenze = fields.Char('Scadenze')

    def uploadInvoice(self):
        xml_invoice = False
        all_invoices = self.env['ir.attachment'].search([('res_id','=', self.id)])
        for invoice in all_invoices:
            if 'xml' in invoice.name:
                xml_invoice = invoice
                break

        self.env['doceasy'].uploadInvoice(xml_invoice)







