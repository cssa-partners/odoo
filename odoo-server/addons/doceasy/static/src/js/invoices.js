odoo.define('doceasy.bills.tree', function (require) {
"use strict";
    var core = require('web.core');
    var ListController = require('web.ListController');
    var ListView = require('web.ListView');
    var viewRegistry = require('web.view_registry');
    var rpc = require('web.rpc');

    var BillsListController = ListController.extend({
        buttons_template: 'BillsListView.buttons',
        events: _.extend({}, ListController.prototype.events, {
            'click .download_invoices': '_onDownloadInvoices'
        }),

        _onDownloadInvoices: function (event) {
            var self = this;
            rpc.query({
                route: '/doceasy/download_invoices'
            })
        },
    });

    var BillsListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: BillsListController,
        }),
    });

    viewRegistry.add('account_tree', BillsListView);
});