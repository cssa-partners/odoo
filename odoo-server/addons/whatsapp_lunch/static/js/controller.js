odoo.define('whatsapp_lunch.controller', function (require) {
"use strict";

var core = require('web.core');
var ListController = require('web.ListController');
var rpc = require('web.rpc');
var session = require('web.session');
var _t = core._t;

ListController.include({
   renderButtons: function($node) {
       this._super.apply(this, arguments);
           if (this.$buttons) {
             this.$buttons.find('.send_lunch').click(this.proxy('_onSendLunchWhatsApp')) ;
           }
       },

       _onSendLunchWhatsApp() {
           return this._rpc({
               model: 'lunch.order',
               method: 'send_order',
               args: ['id'],
           }).then(function(whatsapp) {
               var url = "https://web.whatsapp.com/send?phone=" + whatsapp[0] + "&text=" + whatsapp[1]
               window.open(url);
           })
       }
   })
});