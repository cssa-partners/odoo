# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import time
import datetime
from datetime import timedelta

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class AccountPaymentTermLine(models.Model):
    _inherit = 'account.move'
    scadenze = fields.Char()

    def setNoCashFlow(self):
        self.ref = 'no_cash_flow'