# -*- coding: utf-8 -*-

from odoo import models, fields, api

class FinanziamentiTerzi(models.Model):
    _name = 'finanziamenti_terzi'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class IncassiFinanziamentiPropri(models.Model):
    _name = 'finanziamenti_propri'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class IncassiGestione(models.Model):
    _name = 'incassi_gestione'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class AltreEntrate(models.Model):
    _name = 'altre_entrate'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class TotaleEntrate(models.Model):
    _name = 'totale_entrate'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class Stipendi(models.Model):
    _name = 'stipendi'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class Fornitori(models.Model):
    _name = 'fornitori'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class F24(models.Model):
    _name = 'f24'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class SpeseBancarie(models.Model):
    _name = 'spese_bancarie'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class AltreSpese(models.Model):
    _name = 'altre_spese'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class CompensiRimborsi(models.Model):
    _name = 'compensi_rimborsi'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class RateMutuo(models.Model):
    _name = 'rate_mutuo'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class TotaleUscite(models.Model):
    _name = 'totale_uscite'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class SaldoGestione(models.Model):
    _name = 'saldo_gestione'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class SaldoProgressivo(models.Model):
    _name = 'saldo_progressivo'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class FidoCassa(models.Model):
    _name = 'fido_cassa'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')

class LiquiditaDisponibile(models.Model):
    _name = 'liquidita_disponibile'

    cash_flow_id = fields.Many2one('cash_flow')
    description = fields.Char('Descrizione')
    month_1 = fields.Float('Gen')
    month_2 = fields.Float('Feb')
    month_3 = fields.Float('Mar')
    month_4 = fields.Float('Apr')
    month_5 = fields.Float('Mag')
    month_6 = fields.Float('Giu')
    month_7 = fields.Float('Lug')
    month_8 = fields.Float('Ago')
    month_9 = fields.Float('Set')
    month_10 = fields.Float('Ott')
    month_11 = fields.Float('Nov')
    month_12 = fields.Float('Dec')

    total = fields.Float('Tot')