# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CashFlow(models.Model):
    _name = 'recurrent_payments'

    name = fields.Char(required=True, string='Nome')
    type = fields.Selection([
        ('altre_spese', 'Altre Spese'),
        ('rate_mutuo', 'Rate di mutui o finanziarie')
    ], 'Tipologia', required=True)
    amount = fields.Float(required=True, string='Importo')
    date_from = fields.Date(required=True, string='Inizio')
    date_to = fields.Date(required=True, string='Fine')
    number_of_months = fields.Integer(required=True, string='Cadenza in mesi')