# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CashFlow(models.Model):
    _name = 'cash_flow'

    finanziamenti_terzi = fields.One2many('finanziamenti_terzi', 'cash_flow_id')
    finanziamenti_propri = fields.One2many('finanziamenti_propri', 'cash_flow_id')
    incassi_gestione = fields.One2many('incassi_gestione', 'cash_flow_id')
    altre_entrate = fields.One2many('altre_entrate', 'cash_flow_id')
    totale_entrate = fields.One2many('totale_entrate', 'cash_flow_id')

    stipendi = fields.One2many('stipendi', 'cash_flow_id')
    fornitori = fields.One2many('fornitori', 'cash_flow_id')
    f24 = fields.One2many('f24', 'cash_flow_id')
    spese_bancarie = fields.One2many('spese_bancarie', 'cash_flow_id')
    altre_spese = fields.One2many('altre_spese', 'cash_flow_id')
    compensi_rimborsi = fields.One2many('compensi_rimborsi', 'cash_flow_id')
    rate_mutuo = fields.One2many('rate_mutuo', 'cash_flow_id')
    totale_uscite = fields.One2many('totale_uscite', 'cash_flow_id')

    saldo_gestione = fields.One2many('saldo_gestione', 'cash_flow_id')
    saldo_progressivo = fields.One2many('saldo_progressivo', 'cash_flow_id')
    fido_cassa = fields.One2many('fido_cassa', 'cash_flow_id')
    liquidita_disponibile = fields.One2many('liquidita_disponibile', 'cash_flow_id')

    saldo_iniziazle = fields.Float('Saldo iniziale', required=True)
    name = fields.Char('Name', required=True)

    date_from = fields.Date(required=True)
    date_to = fields.Date(required=True)

    def reload_data(self):
        self.finanziamenti_terzi = False
        self.finanziamenti_propri = False
        self.incassi_gestione = False
        self.altre_entrate = False
        self.totale_entrate = False
        self.stipendi = False
        self.fornitori = False
        self.f24 = False
        self.spese_bancarie = False
        self.altre_spese = False
        self.compensi_rimborsi = False
        self.rate_mutuo = False
        self.totale_uscite = False
        self.saldo_gestione = False
        self.saldo_progressivo = False
        self.fido_cassa = False
        self.liquidita_disponibile = False

        self.find_riba('presentata')
        self.find_riba('da_presentare')
        self.find_purchases()
        self.find_incassi()
        self.generate_finanziamenti_propri()
        self.calculate_stipendi()
        self.generate_f24()
        self.generate_spese_bancarie()
        self.find_recurrent_payments('altre_spese')
        self.find_recurrent_payments('rate_mutuo')
        self.generate_compensi_rimborsi()
        self.generate_fido()

    def calculate_totali(self):
        self.calculate_totale_entrate()
        self.calculate_totale_uscite()
        self.calculate_saldo_gestione()
        self.calculate_saldo_progressivo()
        self.calculate_liqudita()

    def calculate_liqudita(self):
        current_user = self.env.user
        current_company = self.env['res.company'].search([('id','=',current_user.company_id.id)])

        lines = []
        vals = { 'description': 'Totale' }

        vals['month_1'] = self.saldo_progressivo[0].month_1+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_1)+self.fido_cassa[0].month_1
        vals['month_2'] = self.saldo_progressivo[0].month_2+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_2)+self.fido_cassa[0].month_2
        vals['month_3'] = self.saldo_progressivo[0].month_3+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_3)+self.fido_cassa[0].month_3
        vals['month_4'] = self.saldo_progressivo[0].month_4+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_4)+self.fido_cassa[0].month_4
        vals['month_5'] = self.saldo_progressivo[0].month_5+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_5)+self.fido_cassa[0].month_5
        vals['month_6'] = self.saldo_progressivo[0].month_6+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_6)+self.fido_cassa[0].month_6
        vals['month_7'] = self.saldo_progressivo[0].month_7+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_7)+self.fido_cassa[0].month_7
        vals['month_8'] = self.saldo_progressivo[0].month_8+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_8)+self.fido_cassa[0].month_8
        vals['month_9'] = self.saldo_progressivo[0].month_9+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_9)+self.fido_cassa[0].month_9
        vals['month_10'] = self.saldo_progressivo[0].month_10+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_10)+self.fido_cassa[0].month_10
        vals['month_11'] = self.saldo_progressivo[0].month_11+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_11)+self.fido_cassa[0].month_11
        vals['month_12'] = self.saldo_progressivo[0].month_12+(current_company.max_castelletto-self.finanziamenti_terzi[0].month_12)+self.fido_cassa[0].month_12
        vals['total'] = self.saldo_progressivo[0].total+(current_company.max_castelletto-self.finanziamenti_terzi[0].total)+self.fido_cassa[0].total

        lines.append((0, 0, vals))
        self.liquidita_disponibile = lines

    def calculate_saldo_progressivo(self):
        lines = []
        vals = { 'description': 'Totale' }

        saldo = self.saldo_iniziazle-self.totale_uscite[0].month_1+self.totale_entrate[0].month_1
        vals['month_1'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_2)+self.totale_entrate[0].month_2
        vals['month_2'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_3)+self.totale_entrate[0].month_3
        vals['month_3'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_4)+self.totale_entrate[0].month_4
        vals['month_4'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_5)+self.totale_entrate[0].month_5
        vals['month_5'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_6)+self.totale_entrate[0].month_6
        vals['month_6'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_7)+self.totale_entrate[0].month_7
        vals['month_7'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_8)+self.totale_entrate[0].month_8
        vals['month_8'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_9)+self.totale_entrate[0].month_9
        vals['month_9'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_10)+self.totale_entrate[0].month_10
        vals['month_10'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_11)+self.totale_entrate[0].month_11
        vals['month_11'] = saldo
        saldo = (saldo-self.totale_uscite[0].month_12)+self.totale_entrate[0].month_12
        vals['month_12'] = saldo
        saldo = (saldo-self.totale_uscite[0].total)+self.totale_entrate[0].total
        vals['total'] = saldo

        lines.append((0, 0, vals))
        self.saldo_progressivo = lines

    def calculate_saldo_gestione(self):
        lines = []
        vals = {
            'description': 'Totale',
            'month_1': self.totale_entrate[0].month_1-self.totale_uscite[0].month_1,
            'month_2': self.totale_entrate[0].month_2-self.totale_uscite[0].month_2,
            'month_3': self.totale_entrate[0].month_3-self.totale_uscite[0].month_3,
            'month_4': self.totale_entrate[0].month_4-self.totale_uscite[0].month_4,
            'month_5': self.totale_entrate[0].month_5-self.totale_uscite[0].month_5,
            'month_6': self.totale_entrate[0].month_6-self.totale_uscite[0].month_6,
            'month_7': self.totale_entrate[0].month_7-self.totale_uscite[0].month_7,
            'month_8': self.totale_entrate[0].month_8-self.totale_uscite[0].month_8,
            'month_9': self.totale_entrate[0].month_9-self.totale_uscite[0].month_9,
            'month_10': self.totale_entrate[0].month_10-self.totale_uscite[0].month_10,
            'month_11': self.totale_entrate[0].month_11-self.totale_uscite[0].month_11,
            'month_12': self.totale_entrate[0].month_12-self.totale_uscite[0].month_12,
            'total': self.totale_entrate[0].total-self.totale_uscite[0].total
        }

        lines.append((0, 0, vals))
        self.saldo_gestione = lines


    def calculate_totale_entrate(self):
        lines = []
        vals = {
            'description': 'Totale',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        for f_t in self.finanziamenti_terzi:
            vals['month_1'] += f_t.month_1
            vals['month_2'] += f_t.month_2
            vals['month_3'] += f_t.month_3
            vals['month_4'] += f_t.month_4
            vals['month_5'] += f_t.month_5
            vals['month_6'] += f_t.month_6
            vals['month_7'] += f_t.month_7
            vals['month_8'] += f_t.month_8
            vals['month_9'] += f_t.month_9
            vals['month_10'] += f_t.month_10
            vals['month_11'] += f_t.month_11
            vals['month_12'] += f_t.month_12
            vals['total'] += f_t.total

        for f_p in self.finanziamenti_propri:
            vals['month_1'] += f_p.month_1
            vals['month_2'] += f_p.month_2
            vals['month_3'] += f_p.month_3
            vals['month_4'] += f_p.month_4
            vals['month_5'] += f_p.month_5
            vals['month_6'] += f_p.month_6
            vals['month_7'] += f_p.month_7
            vals['month_8'] += f_p.month_8
            vals['month_9'] += f_p.month_9
            vals['month_10'] += f_p.month_10
            vals['month_11'] += f_p.month_11
            vals['month_12'] += f_p.month_12
            vals['total'] += f_p.total

        for i_g in self.incassi_gestione:
            if 'Crediti v/Clienti' not in i_g['description']:
                vals['month_1'] += i_g.month_1
                vals['month_2'] += i_g.month_2
                vals['month_3'] += i_g.month_3
                vals['month_4'] += i_g.month_4
                vals['month_5'] += i_g.month_5
                vals['month_6'] += i_g.month_6
                vals['month_7'] += i_g.month_7
                vals['month_8'] += i_g.month_8
                vals['month_9'] += i_g.month_9
                vals['month_10'] += i_g.month_10
                vals['month_11'] += i_g.month_11
                vals['month_12'] += i_g.month_12
                vals['total'] += i_g.total

        for a_e in self.altre_entrate:
            vals['month_1'] += a_e.month_1
            vals['month_2'] += a_e.month_2
            vals['month_3'] += a_e.month_3
            vals['month_4'] += a_e.month_4
            vals['month_5'] += a_e.month_5
            vals['month_6'] += a_e.month_6
            vals['month_7'] += a_e.month_7
            vals['month_8'] += a_e.month_8
            vals['month_9'] += a_e.month_9
            vals['month_10'] += a_e.month_10
            vals['month_11'] += a_e.month_11
            vals['month_12'] += a_e.month_12
            vals['total'] += a_e.total

        lines.append((0, 0, vals))
        self.totale_entrate = lines

    def calculate_totale_uscite(self):
        lines = []
        vals = {
            'description': 'Totale',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        for s in self.stipendi:
            vals['month_1'] += s.month_1
            vals['month_2'] += s.month_2
            vals['month_3'] += s.month_3
            vals['month_4'] += s.month_4
            vals['month_5'] += s.month_5
            vals['month_6'] += s.month_6
            vals['month_7'] += s.month_7
            vals['month_8'] += s.month_8
            vals['month_9'] += s.month_9
            vals['month_10'] += s.month_10
            vals['month_11'] += s.month_11
            vals['month_12'] += s.month_12
            vals['total'] += s.total

        for f in self.fornitori:
            vals['month_1'] += f.month_1
            vals['month_2'] += f.month_2
            vals['month_3'] += f.month_3
            vals['month_4'] += f.month_4
            vals['month_5'] += f.month_5
            vals['month_6'] += f.month_6
            vals['month_7'] += f.month_7
            vals['month_8'] += f.month_8
            vals['month_9'] += f.month_9
            vals['month_10'] += f.month_10
            vals['month_11'] += f.month_11
            vals['month_12'] += f.month_12
            vals['total'] += f.total

        for ff in self.f24:
            vals['month_1'] += ff.month_1
            vals['month_2'] += ff.month_2
            vals['month_3'] += ff.month_3
            vals['month_4'] += ff.month_4
            vals['month_5'] += ff.month_5
            vals['month_6'] += ff.month_6
            vals['month_7'] += ff.month_7
            vals['month_8'] += ff.month_8
            vals['month_9'] += ff.month_9
            vals['month_10'] += ff.month_10
            vals['month_11'] += ff.month_11
            vals['month_12'] += ff.month_12
            vals['total'] += ff.total

        for s_b in self.spese_bancarie:
            vals['month_1'] += s_b.month_1
            vals['month_2'] += s_b.month_2
            vals['month_3'] += s_b.month_3
            vals['month_4'] += s_b.month_4
            vals['month_5'] += s_b.month_5
            vals['month_6'] += s_b.month_6
            vals['month_7'] += s_b.month_7
            vals['month_8'] += s_b.month_8
            vals['month_9'] += s_b.month_9
            vals['month_10'] += s_b.month_10
            vals['month_11'] += s_b.month_11
            vals['month_12'] += s_b.month_12
            vals['total'] += s_b.total

        for a_s in self.altre_spese:
            vals['month_1'] += a_s.month_1
            vals['month_2'] += a_s.month_2
            vals['month_3'] += a_s.month_3
            vals['month_4'] += a_s.month_4
            vals['month_5'] += a_s.month_5
            vals['month_6'] += a_s.month_6
            vals['month_7'] += a_s.month_7
            vals['month_8'] += a_s.month_8
            vals['month_9'] += a_s.month_9
            vals['month_10'] += a_s.month_10
            vals['month_11'] += a_s.month_11
            vals['month_12'] += a_s.month_12
            vals['total'] += a_s.total

        for c_r in self.compensi_rimborsi:
            vals['month_1'] += c_r.month_1
            vals['month_2'] += c_r.month_2
            vals['month_3'] += c_r.month_3
            vals['month_4'] += c_r.month_4
            vals['month_5'] += c_r.month_5
            vals['month_6'] += c_r.month_6
            vals['month_7'] += c_r.month_7
            vals['month_8'] += c_r.month_8
            vals['month_9'] += c_r.month_9
            vals['month_10'] += c_r.month_10
            vals['month_11'] += c_r.month_11
            vals['month_12'] += c_r.month_12
            vals['total'] += c_r.total

        for r_m in self.rate_mutuo:
            vals['month_1'] += r_m.month_1
            vals['month_2'] += r_m.month_2
            vals['month_3'] += r_m.month_3
            vals['month_4'] += r_m.month_4
            vals['month_5'] += r_m.month_5
            vals['month_6'] += r_m.month_6
            vals['month_7'] += r_m.month_7
            vals['month_8'] += r_m.month_8
            vals['month_9'] += r_m.month_9
            vals['month_10'] += r_m.month_10
            vals['month_11'] += r_m.month_11
            vals['month_12'] += r_m.month_12
            vals['total'] += r_m.total

        lines.append((0, 0, vals))
        self.totale_uscite = lines

    def generate_fido(self):
        lines = []
        vals = {
            'description': '',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }
        lines.append((0, 0, vals))
        self.fido_cassa = lines

    def generate_spese_bancarie(self):
        lines = []
        vals = {
            'description': 'Carte di credito'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Soldo'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Spese bancarie'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Commissioni'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Interessi passivi'
        }
        lines.append((0, 0, vals))
        self.spese_bancarie = lines

    def generate_compensi_rimborsi(self):
        lines = []
        vals = {
            'description': 'Compensi stabiliti'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Rimborsi spese amministratori'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Gettoni di presenza'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'Altro'
        }
        lines.append((0, 0, vals))
        self.compensi_rimborsi = lines

    def find_recurrent_payments(self, type):
        recurrent_payments = self.env['recurrent_payments'].search([('type', '=', type)])
        lines = []

        i = 0
        for payment in recurrent_payments:
            if (self.date_from <= payment.date_from <= self.date_to) or (self.date_from <= payment.date_to <= self.date_to):

                vals = {
                    'description': payment.name,
                    'month_1': 0,
                    'month_2': 0,
                    'month_3': 0,
                    'month_4': 0,
                    'month_5': 0,
                    'month_6': 0,
                    'month_7': 0,
                    'month_8': 0,
                    'month_9': 0,
                    'month_10': 0,
                    'month_11': 0,
                    'month_12': 0,
                    'total': 0
                }

                for _ in range(12):
                    value = payment.date_from.month+i
                    if value < 13:
                        vals['month_'+str(value)] = payment.amount
                    else:
                        i = 0
                    i += payment.number_of_months

                n_payments = round(12/payment.number_of_months,0)
                vals['total'] = payment.amount*n_payments
                lines.append((0, 0, vals))

        if type == 'altre_spese':
            self.altre_spese = lines
        else:
            self.rate_mutuo = lines

    def generate_f24(self):
        lines = []
        vals = {
            'description': 'F24 versato contributi dipendenti'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'F24 versato IMPOSTE'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'F24 versato IVA'
        }
        lines.append((0, 0, vals))
        self.f24 = lines

    def generate_finanziamenti_propri(self):
        lines = []
        vals = {
            'description': 'apporto di denaro da soci per aumenti di capitale'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'prestiti infruttiferi da soci'
        }
        lines.append((0, 0, vals))
        vals = {
            'description': 'altro'
        }
        lines.append((0, 0, vals))
        self.finanziamenti_propri = lines

    def find_purchases(self):
        all_purchases = self.env['account.move'].search(['|',('type','=','in_invoice'),('type','=','in_receipt')])

        lines = []
        for purchase in all_purchases:
            if purchase.ref and 'no_cash_flow' in purchase.ref or purchase.journal_id.name == 'Collaboratori':
                continue

            vals = {
                'description': purchase.partner_id.name,
                'month_1': 0,
                'month_2': 0,
                'month_3': 0,
                'month_4': 0,
                'month_5': 0,
                'month_6': 0,
                'month_7': 0,
                'month_8': 0,
                'month_9': 0,
                'month_10': 0,
                'month_11': 0,
                'month_12': 0,
                'total': 0
            }

            added = False
            for line in purchase.line_ids:
                if line.date_maturity and self.date_from <= line.date_maturity <= self.date_to:
                    vals['month_'+str(line.date_maturity.month)] += line.credit
                    vals['total'] += line.credit

                    if not added:
                        lines.append(vals)
                        added = True


        grouped_partners = []
        for v in lines:
            if len(grouped_partners) == 0:
                grouped_partners.append(v)
                lines.remove(v)
            else:
                found = False
                for g in grouped_partners:
                    if 'SEVENDATA' in g['description']:
                        print("hombre",g)
                    if g['description'] == v['description']:
                        g['month_1'] += v['month_1']
                        g['month_2'] += v['month_2']
                        g['month_3'] += v['month_3']
                        g['month_4'] += v['month_4']
                        g['month_5'] += v['month_5']
                        g['month_6'] += v['month_6']
                        g['month_7'] += v['month_7']
                        g['month_8'] += v['month_8']
                        g['month_9'] += v['month_9']
                        g['month_10'] += v['month_10']
                        g['month_11'] += v['month_11']
                        g['month_12'] += v['month_12']
                        g['total'] += v['total']

                        found = True
                        break

                if not found:
                    grouped_partners.append(v)
                    lines.remove(v)

        grouped_lines = []
        for grouped_partner in grouped_partners:
            grouped_lines.append((0,0,grouped_partner))
        self.fornitori = grouped_lines


    def find_riba(self, type):
        all_riba = self.env['riba'].search([('status','=',type)])
        description = 'Ri.Ba / Anticipo fatture' if type == 'presentata' else 'Incassi da crediti verso clienti'

        vals = {
            'description': description,
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        lines = []
        for riba in all_riba:
            if self.date_from <= riba.due_date <= self.date_to:
                #amount_riba = float(riba.value_amount.replace('€', ''))
                amount_riba = riba.amount
                vals['month_'+str(riba.due_date.month)] += amount_riba
                vals['total'] += amount_riba

                vals['total'] += amount_riba

        lines.append((0, 0, vals))

        if type == 'presentata':
            vals = {
                'description': 'finanziamenti medio-lunghi di liquidità'
            }
            lines.append((0, 0, vals))
            vals = {
                'description': 'altro'
            }
            lines.append((0, 0, vals))
            self.finanziamenti_terzi = lines


    def find_incassi(self):
        lines = []
        invoices = self.env['account.move'].search([('type','=','out_invoice')])

        vals_bonifici = {
            'description': 'Bonifici',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        vals_assegni = {
            'description': 'Assegni',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        vals_riba = {
            'description': 'Riba',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        vals_crediti = {
            'description': 'Crediti v/Clienti',
            'month_1': 0,
            'month_2': 0,
            'month_3': 0,
            'month_4': 0,
            'month_5': 0,
            'month_6': 0,
            'month_7': 0,
            'month_8': 0,
            'month_9': 0,
            'month_10': 0,
            'month_11': 0,
            'month_12': 0,
            'total': 0
        }

        for invoice in invoices:
            if invoice.payment_method and 'Bonifico' in invoice.payment_method:
                for line in invoice.line_ids:
                    if line.date_maturity and self.date_from <= line.date_maturity <= self.date_to:
                        vals_bonifici['month_'+str(line.date_maturity.month)] += line.debit
                        vals_crediti['month_'+str(line.date_maturity.month)] += line.debit
                        vals_bonifici['total'] += line.debit
        lines.append((0, 0, vals_bonifici))

        for invoice in invoices:
            if invoice.payment_method and 'Assegno' in invoice.payment_method:
                for line in invoice.line_ids:
                    if line.date_maturity and self.date_from <= line.date_maturity <= self.date_to:
                        vals_assegni['month_'+str(line.date_maturity.month)] += line.debit
                        vals_crediti['month_'+str(line.date_maturity.month)] += line.debit
                        vals_assegni['total'] += line.debit
        lines.append((0, 0, vals_assegni))

        for invoice in invoices:
            if invoice.payment_method and 'Riba' in invoice.payment_method:
                for line in invoice.line_ids:
                    if line.date_maturity and self.date_from <= line.date_maturity <= self.date_to:
                        vals_riba['month_'+str(line.date_maturity.month)] += line.debit
                        vals_crediti['month_'+str(line.date_maturity.month)] += line.debit
                        vals_riba['total'] += line.debit
        lines.append((0, 0, vals_riba))

        lines.append((0, 0, vals_crediti))

        self.incassi_gestione = lines


    def calculate_stipendi(self):
        lines = []

        all_users = self.env['hr.employee'].search([])
        for user in all_users:
            contract = self.env['hr.contract'].search(['&',('state','=','open'),('employee_id','=',user.id)], limit=1)

            user_vals = {
                'description': user.name,
                'month_1': 0,
                'month_2': 0,
                'month_3': 0,
                'month_4': 0,
                'month_5': 0,
                'month_6': 0,
                'month_7': 0,
                'month_8': 0,
                'month_9': 0,
                'month_10': 0,
                'month_11': 0,
                'month_12': 0,
                'total': 0
            }

            vals = {
                'date_from': self.date_from,
                'date_to': self.date_to,
                'employee_id': user.id
            }
            payslip = self.env['hr.payslip'].create(vals)
            payslip.calculateCommissions()

            for line in payslip.commission_lines:
                if line.state == 'Confermato':
                    if self.date_from.month <= 1 <= self.date_to.month:
                        user_vals['month_1'] += line.month_1
                    if self.date_from.month <= 2 <= self.date_to.month:
                        user_vals['month_2'] += line.month_2
                    if self.date_from.month <= 3 <= self.date_to.month:
                        user_vals['month_3'] += line.month_3
                    if self.date_from.month <= 4 <= self.date_to.month:
                        user_vals['month_4'] += line.month_4
                    if self.date_from.month <= 5 <= self.date_to.month:
                        user_vals['month_5'] += line.month_5
                    if self.date_from.month <= 6 <= self.date_to.month:
                        user_vals['month_6'] += line.month_6
                    if self.date_from.month <= 7 <= self.date_to.month:
                        user_vals['month_7'] += line.month_7
                    if self.date_from.month <= 8 <= self.date_to.month:
                        user_vals['month_8'] += line.month_8
                    if self.date_from.month <= 9 <= self.date_to.month:
                        user_vals['month_9'] += line.month_9
                    if self.date_from.month <= 10 <= self.date_to.month:
                        user_vals['month_10'] += line.month_10
                    if self.date_from.month <= 11 <= self.date_to.month:
                        user_vals['month_11'] += line.month_11
                    if self.date_from.month <= 12 <= self.date_to.month:
                        user_vals['month_12'] += line.month_12

            if self.date_from.month <= 1 <= self.date_to.month:
                user_vals['month_1'] += contract.wage
            if self.date_from.month <= 2 <= self.date_to.month:
                user_vals['month_2'] += contract.wage
            if self.date_from.month <= 3 <= self.date_to.month:
                user_vals['month_3'] += contract.wage
            if self.date_from.month <= 4 <= self.date_to.month:
                user_vals['month_4'] += contract.wage
            if self.date_from.month <= 5 <= self.date_to.month:
                user_vals['month_5'] += contract.wage
            if self.date_from.month <= 6 <= self.date_to.month:
                user_vals['month_6'] += contract.wage
            if self.date_from.month <= 7 <= self.date_to.month:
                user_vals['month_7'] += contract.wage
            if self.date_from.month <= 8 <= self.date_to.month:
                user_vals['month_8'] += contract.wage
            if self.date_from.month <= 9 <= self.date_to.month:
                user_vals['month_9'] += contract.wage
            if self.date_from.month <= 10 <= self.date_to.month:
                user_vals['month_10'] += contract.wage
            if self.date_from.month <= 11 <= self.date_to.month:
                user_vals['month_11'] += contract.wage
            if self.date_from.month <= 12 <= self.date_to.month:
                user_vals['month_12'] += contract.wage

            user_vals['total'] +=  user_vals['month_1'] + \
                                   user_vals['month_2']+ \
                                   user_vals['month_3'] + \
                                   user_vals['month_4'] + \
                                   user_vals['month_5'] + \
                                   user_vals['month_6'] + \
                                   user_vals['month_7'] + \
                                   user_vals['month_8'] + \
                                   user_vals['month_9'] + \
                                   user_vals['month_10'] + \
                                   user_vals['month_11'] + \
                                   user_vals['month_12']


            lines.append((0, 0, user_vals))
            payslip.unlink()

        self.stipendi = lines
