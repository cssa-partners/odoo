# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Company(models.Model):
    _inherit = 'res.company'

    castelletto = fields.Integer('Castelletto')
    max_castelletto = fields.Integer('Castelletto')

    @api.onchange('castelletto')
    def _onchange_castelletto(self):
        self._origin.max_castelletto = self.castelletto

        all_riba = self.env['riba'].search([])
        for riba in all_riba:
            riba.castelletto = '€ ' + str(self.castelletto)
