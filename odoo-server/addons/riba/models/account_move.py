# -*- coding: utf-8 -*-

from odoo import models, fields, api
import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class Account(models.Model):
    _inherit = 'account.move'

    def action_set_up_riba(self):
        current_company = self.env['res.company'].search([('id','=',self.user_id.company_id.id)])

        if current_company.castelletto <= current_company.max_castelletto:
            vals = {}
            sale_order = self.env['sale.order'].search([('invoice_ids','=',self.id)])

            counter = 0
            n_installments = len(sale_order.installment_lines)-1 if sale_order.payment_term_id.has_anticipo else len(sale_order.installment_lines)
            for line in sale_order.installment_lines:
                if not sale_order.payment_term_id.has_anticipo or (sale_order.payment_term_id.has_anticipo and counter > 0):
                    vals['due_date'] = line.due_date
                    #vals['value_amount'] = line.value_amount
                    vals['amount'] = float((line.value_amount).replace('€', ''))
                    vals['account_move_id'] = self.id
                    #vals['castelletto'] = '€ ' + str(self.company_id.castelletto)
                    vals['status'] = 'da_presentare'

                    riba = self.env['riba'].create(vals)
                    riba.company_id = self.company_id
                    riba.partner_id = self.partner_id
                    riba.invoice_id = self.id
                    riba.invoice_n = self.name
                    riba.n_riba = str(counter)+"/"+str(n_installments)

                counter += 1

            return True

    def migrate_riba_amount(self):
        thread = threading.Thread(target=self.migrate, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_riba = new_env['riba'].search([])

                for riba in all_riba:
                    riba.amount = float((riba.value_amount).replace('€', ''))
