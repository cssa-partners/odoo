# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Riba(models.Model):
    _name = 'riba'

    company_id = fields.Many2one('res.company')
    partner_id = fields.Many2one('res.partner', 'Cliente')
    #value_amount = fields.Char('Importo')
    amount = fields.Float('Importo')
    due_date = fields.Date('Scadenza')
    account_move_id = fields.Integer()
    status = fields.Selection([('da_presentare','DA PRESENTARE'),('insoluta','INSOLUTA'),('sospesa','SOSPESA'),('pagato','PAGATA'),('presentata','PRESENTATA')],'Stato')
    #castelletto = fields.Char('Castelletto')
    invoice_id = fields.Integer()
    invoice_n = fields.Char('N° fattura')
    n_riba = fields.Char()
    presented_date = fields.Date('Data presentata')

    def pay_riba(self, date):
        current_company = self.env['res.company'].search([('id','=',self.company_id.id)])

        max_castelletto = round(current_company.max_castelletto,2)
        castelletto = round(current_company.castelletto,2)
        #value_amount = float(self.value_amount.replace('€', ''))

        if (self.status == 'presentata' or self.status == 'sospesa') and max_castelletto >= (castelletto+self.amount):
            #self.castelletto = '€ ' + str(round((castelletto + value_amount),2))
            castelletto += self.amount

            current_company.castelletto = round(castelletto, 2)

            journal_id = self.env['account.journal'].search([('name','=','Banca')]).id
            payment_method_id = self.env['account.payment.method'].search(['&',('name','=','Riba'),('payment_type','=','inbound')]).id
            vals = {}
            vals['invoice_ids'] = self.env['account.move'].search([('id','=',self.account_move_id)])
            vals['amount'] = self.amount
            vals['payment_method_id'] = payment_method_id
            vals['currency_id'] = 1
            vals['payment_type'] = 'inbound'
            vals['partner_id'] = self.partner_id.id
            vals['partner_type'] = 'customer'
            vals['journal_id'] = journal_id
            vals['payment_date'] = date

            account_payment = self.env['account.payment'].create(vals)
            account_payment.post()

            self.status = 'pagato'


    def set_status(self, status, date):
        if date:
            self.presented_date = date

        current_company = self.env['res.company'].search([('id','=',self.company_id.id)])
        castelletto = round(current_company.castelletto,2)
        max_castelletto = round(current_company.max_castelletto,2)

        #riba_amount = float(self.value_amount.replace('€', ''))
        if (castelletto-self.amount) >= 0 and status == 'presentata':
            self.status = status
            current_company.castelletto -= self.amount
            #self.castelletto = '€ ' + str(round((castelletto - riba_amount),2))
        elif max_castelletto >= (castelletto+self.amount) and status == 'insoluta':
            self.status = status
            current_company.castelletto += self.amount
        elif status == 'sospesa':
            self.status = status

    def action_set_date_presentata(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Imposta Data',
            'res_model': 'riba.wizard',
            'target': 'new',
            'view_id': self.env.ref('riba.riba_wizard').id,
            'view_mode': 'form',
            'context': {
                'default_riba_id': self.id,
                'default_riba_state': 'presentata'
            }
        }

    def action_set_date_paid(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Imposta Data',
            'res_model': 'riba.wizard',
            'target': 'new',
            'view_id': self.env.ref('riba.riba_wizard').id,
            'view_mode': 'form',
            'context': {
                'default_riba_id': self.id,
                'default_riba_state': 'paid'
            }
        }

    def view_invoice(self):
        return {
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.invoice_id,
            'target': 'current',
            'flags': {
                'mode': 'readonly'
            }
        }

class RibaWizard(models.TransientModel):
    _name = 'riba.wizard'

    riba_id = fields.Integer()
    riba_state = fields.Char()
    date = fields.Date()

    def action_set_date(self):
        current_riba = self.env['riba'].search([('id','=',self.riba_id)])
        if self.riba_state == 'paid':
            current_riba.pay_riba(self.date)
        else:
            current_riba.set_status(self.riba_state, self.date)
