# -*- coding: utf-8 -*-
# Part of SnepTech. See LICENSE file for full copyright and licensing details.##
###############################################################################

from odoo import api, fields, models, _
import requests
from odoo import tools

class res_partner(models.Model):
    _inherit = 'res.partner'

    latitude = fields.Char(string='Latitude')
    longitude = fields.Char(string='Longitude')
    current_location = fields.Boolean(string='Current Location', default=True)
    partner_map_ids = fields.One2many('map.url.spt', 'partner_id', string="Partner map")

    @api.model
    def create(self, vals):
        res = super(res_partner, self).create(vals)
        res.partner_map_ids = [(0,0,{
            'partner_id':res.id,         
        })]
        return res

    def unlink(self):
        for record in self:
            record.partner_map_ids.unlink()
        return super(res_partner, self).unlink()

    def search_location(self):
        for record in self:
            print("/////////////////////////////////")
            print(record)
            print("/////////////////////////////////")
    
    def geo_query_address(self, zip=None, city=None, state=None, country=None):
        map_obj = self.env['map.url.spt']
        if map_obj.server_connection():
            method = map_obj.get_method('geo_query_address')
            if method['method']:
                localdict = {'self': self, 'user_obj': self.env.user, 'zip':zip, 'city':city, 'state':state, 'country':country}
                exec(method['method'], localdict)
                zip = localdict['zip']
                city = localdict['city']
                state = localdict['state']
                country = localdict['country']
        return tools.ustr('+'.join(filter(None, [("%s %s" % (zip or '', city or '')).strip(),
                                                state,
                                                country])))

    def geo_find(self,addr):
        map_obj = self.env['map.url.spt']
        if map_obj.server_connection():
            method = map_obj.get_method('geo_find')
            if method['method']:
                localdict = {'self': self, 'user_obj': self.env.user, 'addr':addr}
                exec(method['method'], localdict)
        result_url = localdict['result_url']
        try:
            if result_url:
                return float(result_url[0]['lat']), float(result_url[0]['lon'])
        except (KeyError, ValueError):
            return None


    @api.onchange('street', 'street2', 'city', 'state_id', 'zip', 'country_id')
    def _onchange_lat_long(self):
        map_obj = self.env['map.url.spt']
        if map_obj.server_connection():
            method = map_obj.get_method('_onchange_lat_long')
            if method['method']:
                localdict = {'self': self, 'user_obj': self.env.user}
                exec(method['method'], localdict)

    def get_geo_location(self):
        map_url_obj = self.env['map.url.spt']
        temp = self._onchange_lat_long()
        print('\n test=',temp,'\n')
        for record in self:
            record.partner_map_ids.unlink()
            map_url_obj.create({'partner_id':record.id})
        return True
    
    def show_partner_location(self):
        self.ensure_one()
        self.get_geo_location()
        url = self.partner_map_ids.url
        return {
            'type': 'ir.actions.act_url',
            'name': "Location",
            'target': 'new',
            'url': url,
        }


    def view_geo_location(self):
        map_obj = self.env['map.url.spt']
        if map_obj.server_connection():
            method = map_obj.get_method('view_geo_location')
            if method['method']:
                localdict = {'self': self, 'user_obj': self.env.user}
                exec(method['method'], localdict)

        return {
            'name': _('Location'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'geo.map.spt',
            'res_id':localdict['geo_rec'].id,
            'views': [(localdict['form_view'].id, 'form')],
            'target': 'current',
        }

    def view_geo_partner_location(self):
        map_obj = self.env['map.url.spt']
        if map_obj.server_connection():
            method = map_obj.get_method('view_geo_partner_location')
            if method['method']:
                localdict = {'self': self, 'user_obj': self.env.user}
                exec(method['method'], localdict)
        # print('\n',localdict['url'],'\n')
        
        return {
            'type': 'ir.actions.act_url',
            'name': "Location",
            'target': 'new',
            'url': localdict['url'],
        }

    def open_partner(self):
        self.ensure_one()
        form_view = self.env.ref('base.view_partner_form')
        
        return {
            'name': _('Partner'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'res_id':self.id,
            'views': [(form_view.id, 'form')],
            'target': 'current',
        }
        
