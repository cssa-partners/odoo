# -*- coding: utf-8 -*-
# Part of SnepTech. See LICENSE file for full copyright and licensing details.##
###############################################################################

from odoo import api, fields, models, _
from datetime import datetime

class geo_map_spt(models.Model):
    _name = 'geo.map.spt'
    _description = 'Geo Map'

    name = fields.Char('Name', default="Location" )

    partner_ids = fields.Many2many('res.partner', 'geo_map_partner_rel_spt', 'geo_map_spt', 'partner_id', string="Partners")
    map_url_ids = fields.Many2many('map.url.spt', 'geo_map_map_url_rel_spt', 'geo_map_spt', 'map_url_id', string='Map URL')

    def auto_map_delete(self):
        for record in self.search([]):
            remaining = datetime.today() - record.create_date
            if remaining.days >= 7:
                record.unlink()