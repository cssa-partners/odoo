# -*- coding: utf-8 -*-
# Part of SnepTech See LICENSE file for full copyright and licensing details.##
##################################################################################

from . import res_partner
from . import map_url_spt
from . import geo_map_spt
