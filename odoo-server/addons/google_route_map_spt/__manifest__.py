
# -*- coding: utf-8 -*-
# Part of SnepTech See LICENSE file for full copyright and licensing details.##
##################################################################################

{
    'name': "Google Route Search",
    'summary': "Google Route Search",
    'sequence':1,
    "price": '99.99',
    "currency": 'USD',
       
    'description':""" 
        Get your direction to your customer's location on google map in odoo.
        
        View your customer's location on embeded google map. No google API is needed.
        
    """,
    'category': '',
    'version': '13.0.0.1',
    'license': 'AGPL-3',
    'author': 'SnepTech',
    'website': 'https://www.sneptech.com',
    
    'depends': ['base'],

    'data': [
        'security/ir.model.access.csv',
        'data/action_spt.xml',
        'data/cron_spt.xml',
        'views/res_partner_view.xml',
        'views/map_url_spt_view.xml',
        'views/geo_map_spt_view.xml',
    ],
            
    'application': True,
    'installable': True,
    'auto_install': False,
    "images":['static/description/Banner.png'],
    'live_test_url':"https://youtu.be/1Q0THkbh6ow",
}
