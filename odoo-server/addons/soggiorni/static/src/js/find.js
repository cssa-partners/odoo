odoo.define('strutture.tree', function (require) {
"use strict";
    var core = require('web.core');
    var ListController = require('web.ListController');
    var ListView = require('web.ListView');
    var viewRegistry = require('web.view_registry');
    var rpc = require('web.rpc');


    var StruttureListController = ListController.include({
        events: _.extend({}, ListController.prototype.events, {
            'click .button_find': '_onFindPlaces'
        }),
        _onFindPlaces: function (event) {
            var self = this;

            event.preventDefault()
            self.do_action({
                name: 'Inserisci nuova destinazione',
                type: 'ir.actions.act_window',
                res_model: 'destination.wizard',
                view_mode: 'form',
                view_type: 'form',
                views: [[false, 'form']],
                target: 'new'
            })
        }
    });
});