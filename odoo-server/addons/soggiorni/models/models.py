# -*- coding: utf-8 -*-

from odoo import models, fields, api
import requests

class soggiorni(models.Model):
    _name = 'soggiorni'

    check_in = fields.Date('Check In')
    check_out = fields.Date('Check Out')

    persona = fields.Many2one('res.users', string='Persona', index=True, tracking=True)
    struttura = fields.Many2one('res.partner', string='Struttura', index=True, tracking=True, domain="['&',('is_company','=',True),('default_product.name','ilike','HOTEL')]")


class Partner(models.Model):
    _inherit = 'res.partner'

    distance = fields.Float(string='Distanza')

    def resetDistance(self):
        all_hotels = self.env['res.partner'].search(['&',('is_company','=',True),('default_product.name','ilike','HOTEL')])
        for hotel in all_hotels:
            hotel.distance = 0

class DestinationWizard(models.TransientModel):
    _name = 'destination.wizard'

    city = fields.Char('Città')

    def action_calculate_distance(self):
        all_hotels = self.env['res.partner'].search(['&',('is_company','=',True),('default_product.name','ilike','HOTEL')])
        for hotel in all_hotels:
            if hotel.city and hotel.street and hotel.zip:
                hotel_address = hotel.city+","+hotel.street+","+hotel.zip
                hotel.distance = self.calculate_km(self.city+",IT", hotel_address)

    def calculate_km(self, origin, destination):
        km = 0
        googleAPIKey = "AIzaSyA0O79j0SC2rCzSGV_5KcsuUQesEMJ7Q0Y"

        google_distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin+"&destinations="+destination+"&key="+googleAPIKey
        #print(google_distance_url)
        google_distance_result = requests.get(google_distance_url)
        distance_km_json = google_distance_result.json()
        distance_km_rows = distance_km_json['rows']
        print(distance_km_rows)
        for row in distance_km_rows:
            for element in row['elements']:
                try:
                    distance_km_txt = element['distance']['text']
                    distance_km_txt = distance_km_txt.replace('km','')
                    distance_km_txt = distance_km_txt.replace('m','')
                    km = distance_km_txt
                    #km = re.sub("[^0-9]", "", distance_km_txt)
                    #print("Km",km)
                    break
                except:
                    break
            break

        return km